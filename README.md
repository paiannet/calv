# Repository for avl-trees benchmark comparison

_author_ : Paul Iannetta  
_Context_ : PhD Thesis "optimisation of complex data structures"  
_Projet_ : [CODAS](https://codas.ens-lyon.fr)

## Implementation

This repository implements :
  * AVL Trees (`include/avl-tree.h`)
  * Tarbres (AVL trees stored according to the DFS layout) (`include/avl-bf.h`)
as header only cpp libraries.

## Benchmarks

Apart from unit tests, new benchmarks are proposed:
  * `ocaml-typing-log` : instrumentation of the ocaml typer phase to produce tree operation log.
  * `database` : bdd log coming from lldb database which is replayed against our implementation.
  * `compress` : benchmarks the performance of tarbres with different compression thresolds.
  * `insert` : benchmarks sequential and parallel implementation of the insert operation on tarbre.
  * `map` : benchmarks the application of a function over a tarbre. That is apply `f` to all elements.
  * `parallel-memcpy` : benchmarks which tries to find the best value way to cut a big memcpy into many parallel smaller memcpy
  * `pull-down` : benchmarks different pull-down strategies.
  * `pull-up` : benchmarks different pull-up strategies.
  * `shift` : benchmarks different shift strategies.

+ All tests produces csv files that can be later used to produce graphs. More
  details is found in each sub-directory of the test directory.


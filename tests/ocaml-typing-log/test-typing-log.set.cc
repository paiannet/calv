#include <iostream>
#include <sstream>
#include <cstring>
#include <vector>
#include <set>

#include <map>
#include <str.h>

// todo: rewrite with lex/yacc
int
main () {

  std::map<int, std::set<str>> avls;
  std::string cur_line, token, filename;

  /* Preamble */
  while (std::getline(std::cin, cur_line)) {
    std::istringstream iss(cur_line);
    iss >> token;
    if (token != "New") {
      // std::cerr << "Expected token: New" << std::endl;
      return 1;
    }

    iss >> token;
    if (token != "file" && token != "File") {
      // std::cerr << "Expected token: `file` or `File`, got: `" << token << "`." << std::endl;
      return 1;
    }

    iss >> filename;
    if (filename == "") {
      // std::cerr << "Expected filename" << std::endl;
    }

    // std::cout << "Processing the log relative to: " << filename << std::endl;
    while (std::getline(std::cin, cur_line)) {
      std::istringstream iss(cur_line);
      std::string token;

      if (std::getline(iss, token, '(')) {
        if (token == "add" || token == "addl") {
          if (std::getline(iss, token, ')')) {
            std::istringstream args(token);
            std::string token;

            int tree_id = -1;
            if (std::getline(args, token, ',')) {
              try {
                tree_id = std::stoi(token);
              } catch (std::exception e) {
                tree_id = -1;
                continue;
              }
            }

            if (tree_id == -1) {
              // std::cerr << "id has to be positive.\n";
            }

	    std::set<str> current_tree;
            if (tree_id == 0) {
              /* Nothing to do, start afresh */
            } else {
              /* copy */
              current_tree = avls[tree_id];
            }

            while (std::getline(args, token, ',')) {
              // std::cout << "add: " << token << ".\n";
              current_tree.insert(str(token.c_str()));
            }

            iss.ignore(3); // Eats ' = '
            if (std::getline(iss, token, '[')) {
              int new_tree_id = -1;
              // std::cout << "new tree id: " << token << ".\n";
              try {
                new_tree_id = std::stoi(token);
              } catch (std::exception e) {
                tree_id = -1;
                // std::cerr << "aborting: new_tree_id should be a positive number.\n";
                continue;
              }
              avls[new_tree_id] = current_tree;
            }
          } else {
            // std::cerr << "Expected ')'\n";
          }
        } else if (token == "free") {
          if (std::getline(iss, token, ')')) {
            int tree_id = -1;
            if (token == "") {
              // std::cerr << "Expected tree id\n";
            } else {
              try {
                tree_id = std::stoi(token);
              } catch (std::exception e) {
                tree_id = -1;
                // std::cerr << "aborting: new_tree_id should be a positive number.\n";
                continue;
              }
              avls.erase(tree_id);
              // std::cerr << "Tree '" << tree_id << "' has been freed.\n";
            }
          } else {
            // std::cerr << "Expected tree id (followed by closing paren)\n";
          }
        } else if (token == "find") {
          if (std::getline(iss, token, ')')) {
            std::istringstream args(token);
            std::string token;

            int tree_id = -1;
            if (std::getline(args, token, ',')) {
              try {
                tree_id = std::stoi(token);
              } catch (std::exception e) {
                tree_id = -1;
                continue;
              }
            }
            if (tree_id == -1) {
              // std::cerr << "id has to be positive.\n";
            }

            while (std::getline(args, token, ',')) {
              // std::cout << "look up " << token << ": ";
              if (avls[tree_id].find(str(token.c_str())) != avls[tree_id].end()) {
                // std::cout << "found.\n";
              } else {
                // std::cout << "not found.\n";
              }
            }
          }
        } else {
          std::istringstream cmd(token);
          std::string token;
          cmd >> token;
          if (token == "Closing") {
            cmd >> token;
            if (token != "File") {
              // std::cerr << "Expected token: File.\n";
            } else {
              cmd >> token;
              // std::cout << "end the proccessing of file " << token << "\n";
              break;
            }
          } else {
            // std::cerr << "Unsupported operation: " << token << "\n";
          }
        }
      }
    }
  }

  return 0;
}

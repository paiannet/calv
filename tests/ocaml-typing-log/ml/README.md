# ocaml-typing-log

This tools post-process the log produced by the OCaml's typer once it has been
instrumented by `ident.ml.patch`.

## How to make the OCaml compiler produce a log

Apply the patch `ident.ml.patch` to the file `ident.ml` found in the folder
`typing`.

```sh
make world.opt
make bootstrap
```

Now, we can remove for example the standard library and get the compiler to
produce a log when it types it.

```sh
# give a name to the log
export DUMPTREEOP=$PWD/stdlib.log
# flush the stdlib
cd stdlib
make clean
cd ..
# rebuild
make world.opt
```

## How to use the log preprocessor

### Installation

Go into the source directory

```sh
opam install --deps .
dune build
dune exec tarbre_log -- input output
```

In the folder `examples/` there are two files, `dumptreeop.txt` (the log
produced by the ocaml typer) and `dumptreepost.txt` (the log once preprocessed
by the tool).

## How to read the log

- `add(tree_id, symbol) = new_tree_id [new_tree_bytes_size]`
- `addl(tree_id, symbols) = new_tree_id [new_tree_bytes_size]`
- `free(tree_id)` : means that `tree_id` won't be used anymore
- `find(tree_id, symbol)`

`add(0, ...) = n[...]` means that we're constructing a fresh tree that we'll be
called `n`.

## Authors

  * Gabriel Radanne

#include <cstring>
#include <iostream>
#include <chrono>
#include <queue>

#include <pthread.h>

using time_unit = std::chrono::microseconds;

#define LEFT 0
#define RIGHT 1

#define SZ (1 << 10)

#define NB_TIMES 1000
#define BLOCK_STEP (1 << 10)
int chunk = (1 << 20);

struct job {
  int src, dst, sz, *arr;
  job()
    : src(0), dst(0), sz(0), arr(nullptr)
  {};
  job(int src_, int dst_, int sz_, int * arr_)
    : src(src_), dst(dst_), sz(sz_), arr(arr_)
  {};
};

pthread_cond_t impulse;
pthread_mutex_t lock;

int nb_jobs_done = 0;
int time_to_die = 0;
int sleeping_threads = 0;

pthread_t threads[NB_THREADS];
int thread_has_done[NB_THREADS] = {0};
int nb[NB_THREADS] = {0};

std::queue<job> jobs;

struct tarbre {
  int * values;
  int * heights;
  int len;
  int size;
};

static inline int
_greatest_bit_pos(unsigned int n)
{
  return 8*sizeof(n) - __builtin_clz(n);
}

#define max(a,b) ((a > b) ? (a) : (b))

inline int
go_1based(int base, int dir, int times)
{
  if (times < 0) {
    return max(0, base >> (- times));
  } else {
    return ((base + dir) << times) - dir;
  }
}

inline int
go_0based(int base, int dir, int times)
{
  return go_1based(base + 1, dir, times) - 1;
}

void pull_up (int idx, int * arr, int len) {
  // int idx = 2;
  // int len = 1 << (sz + 1);
  int start_lvl = _greatest_bit_pos(idx + 1);
  int end_lvl   = _greatest_bit_pos(len - 1);
  int n         = end_lvl - start_lvl;
  // printf("sl: %d ; el: %d\n", start_lvl, end_lvl);

  /* 1. Compute the direction.
     Are we pulling-up a left sub-tree or a right sub-tree?
     0: left ; 1: right */
  int dir = (idx + 1) & 1;
  for (int i = 0 ; i <= n ; ++i) {
    int dst = go_0based(idx, dir, i - 1);
    int src = go_0based(idx, dir, i);
    // printf("arr[%d] = array[%d] (%d <- %d)\n", dst, src, array[dst], array[src]);
    arr[dst] = arr[src];
  }

  // i is the layer number plus 1.
  for (int i = 0 ; i < n ; ++i) {
    for (int j = 0 ; j < n - i ; ++j) {
      int dst = go_0based(go_0based(go_0based(idx, dir, j - 1), 1 - dir, 1), LEFT, i);
      int src = go_0based(go_0based(go_0based(idx, dir, j), 1 - dir, 1), LEFT, i);
      int size = (1 << i) * sizeof (*arr);
      // printf("write %d bytes from %d to %d\n", (1 << i), src_layer_start, dst_layer_start);
      // 4.1.1 Create copy jobs of size `chunk`.
      for (int k = 0 ; k < size / chunk ; ++k) {
	// fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
	//   src + j * chunk / (sizeof *arr),
	//   dst + j * chunk / (sizeof * arr),
	//   chunk);
	job job(src + k * chunk / (sizeof * arr),
		dst + k * chunk / (sizeof * arr),
		chunk,
                arr);

	pthread_mutex_lock(&lock);
	jobs.push(job);
	// std::cout << "sleeping threads: " << sleeping_threads << std::endl;

	if (sleeping_threads > 0) {
	  // std::cout << "sending impulse" << std::endl;
	  pthread_cond_signal(&impulse);
	  sleeping_threads -= 1;
	}
	pthread_mutex_unlock(&lock);
      }
      // 4.1.2 Create a copy job for the leftover
      int leftover = (size / chunk) * chunk;
      job job(src  + leftover / (sizeof * arr),
	      dst  + leftover / (sizeof * arr),
	      size - leftover,
              arr);
      /* fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n", */
      /*   src + leftover / (sizeof * arr), */
      /*   dst + leftover / (sizeof * arr), */
      /*   size - leftover); */
      pthread_mutex_lock(&lock);
      jobs.push(job);

      if (sleeping_threads > 0) {
	pthread_cond_signal(&impulse);
	sleeping_threads -= 1;
      }
      pthread_mutex_unlock(&lock);
    }
  }

  // 6. scheduling point:
  // wait for all jobs to be done before cleaning.
  while (true) {
    int no_more_jobs;
    pthread_mutex_lock(&lock);
    no_more_jobs = jobs.empty();
    pthread_mutex_unlock(&lock);
    if (no_more_jobs) break;
  }

  // Clean up, memset 0 deepest level below (idx + 1) / 2 - 1
  /* printf("clear %d with %d zeroes\n", */
  /*     (1 << (end_lvl - start_lvl + 1)) * ((idx + 1) / 2) - 1, */
  /*     (1 << (end_lvl - start_lvl))); */
  memset(arr + (1 << (end_lvl - start_lvl + 1)) * ((idx + 1) / 2) - 1,
	 0,
	 (1 << (end_lvl - start_lvl + 1)) * (sizeof *arr));
}

void
pull_down(int idx, int dir, int * arr, int len) {
      int start_lvl = _greatest_bit_pos(idx + 1);
      int end_lvl   = _greatest_bit_pos(len - 1);
      int n = end_lvl - start_lvl - 1;

      // printf("dir = %d\n", dir);
      // printf("sl: %d ; el: %d\n", start_lvl, end_lvl);
      for (int i = n ; i >= 0 ; --i) {
        int src = go_0based(idx, dir, i);
        int dst = go_0based(idx, dir, i + 1);
	// printf("arr[%d] = array[%d]\n", dst, src);
	arr[dst] = arr[src];
      }
      // i is the layer number plus 1.
      for (int i = 0 ; i < n ; ++i) {
	// j walks the layer labeled layer number.
	for (int j = 0 ; j < n - i; j++) {
	  int src = (1 << i) * (2 * ((idx + dir + 1) * (1 << (n - (i + j + 1))) - dir) + (1 - dir)) - 1;
	  int dst = (1 << i) * (2 * ((idx + dir + 1) * (1 << (n - (i + j)))     - dir) + (1 - dir)) - 1;
	  int size = (1 << i) * sizeof (*arr);
	  // printf("write %d bytes from %d to %d\n", (1 << i), src_layer_start, dst_layer_start);
	  for (int k = 0 ; k < size / chunk ; ++k) {
	    // fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
	    //   src + j * chunk / (sizeof *arr),
	    //   dst + j * chunk / (sizeof * arr),
	    //   chunk);
	    job job(src + k * chunk / (sizeof * arr),
		    dst + k * chunk / (sizeof * arr),
		    chunk,
                    arr);

	    pthread_mutex_lock(&lock);
	    jobs.push(job);
	    // std::cout << "sleeping threads: " << sleeping_threads << std::endl;

	    if (sleeping_threads > 0) {
	      // std::cout << "sending impulse" << std::endl;
	      pthread_cond_signal(&impulse);
	      sleeping_threads -= 1;
	    }
	    pthread_mutex_unlock(&lock);
	  }
          int leftover = (size / chunk) * chunk;
          job job(src  + leftover / (sizeof * arr),
                  dst  + leftover / (sizeof * arr),
                  size - leftover,
                  arr);
          /* fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n", */
          /*   src + leftover / (sizeof * arr), */
          /*   dst + leftover / (sizeof * arr), */
          /*   size - leftover); */
          pthread_mutex_lock(&lock);
          jobs.push(job);

          if (sleeping_threads > 0) {
            pthread_cond_signal(&impulse);
            sleeping_threads -= 1;
          }
          pthread_mutex_unlock(&lock);
        }
      }

      while (true) {
        int no_more_jobs;
        pthread_mutex_lock(&lock);
        no_more_jobs = jobs.empty();
        pthread_mutex_unlock(&lock);
        if (no_more_jobs) break;
      }
      /* Clean up */
      arr[(idx + 1) - 1] = 0;
      for (int i = 0 ; i < end_lvl - start_lvl ; ++i) {
        int depth = 1 << i;
        int size  = depth * (sizeof *arr);
        int dest  = depth * (2 * (idx + 1) + (1 - dir)) - 1;

        memset(arr + dest, 0, size);
      }
}

void
shift(int idx, int dir, int * arr, int len) {
  // int idx = 1;
  // int len_ = 1ULL << (sz + 1);
  int start_lvl = _greatest_bit_pos(idx + 1);
  int end_lvl   = _greatest_bit_pos(len - 1);
  int steps     = end_lvl - start_lvl + 1;
  int dir_coef  = 2 * dir - 1; /* 0 -> -1 ; 1 -> 1 */

  // 4. move a layer
  for (int i = 0 ; i < steps ; ++i) {
    int depth = 1 << i;
    int size  = depth * (sizeof *arr);
    int src   = depth * (idx + 1) - 1;
    int dst   = src + dir_coef * depth;

    // 4.1 Create copy jobs of size `chunk`.
    for (int j = 0 ; j < size / chunk ; ++j) {
      // fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
      //   src + j * chunk / (sizeof *arr),
      //   dst + j * chunk / (sizeof * arr),
      //   chunk);
      job job(src + j * chunk / (sizeof * arr),
	      dst + j * chunk / (sizeof * arr),
	      chunk,
              arr);
      pthread_mutex_lock(&lock);
      jobs.push(job);
      // std::cout << "sleeping threads: " << sleeping_threads << std::endl;

      if (sleeping_threads > 0) {
        // std::cout << "sending impulse" << std::endl;
        pthread_cond_signal(&impulse);
        sleeping_threads -= 1;
      }
      pthread_mutex_unlock(&lock);
    }
    // 4.2 Create a copy job for the leftover
    int leftover = (size / chunk) * chunk;
    job job(src + leftover / (sizeof * arr),
	    dst + leftover / (sizeof * arr),
	    size - leftover,
            arr);
    // fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
    //   src + leftover / (sizeof * arr),
    //   dst + leftover / (sizeof * arr),
    //   size - leftover);
    pthread_mutex_lock(&lock);
    jobs.push(job);
    // std::cout << "sleeping threads: " << sleeping_threads << std::endl;

    if (sleeping_threads > 0) {
      // std::cout << "sending impulse" << std::endl;
      pthread_cond_signal(&impulse);
      sleeping_threads -= 1;
    }
    pthread_mutex_unlock(&lock);

    // 4.3 scheduling point:
    // wait for all jobs to be done.
    while (true) {
      int no_more_jobs;
      pthread_mutex_lock(&lock);
      no_more_jobs = jobs.empty();
      pthread_mutex_unlock(&lock);
      if (no_more_jobs) break;
    }
  }
}

int
_balance(int idx, int * heights, int len)
{
  int left = go_0based(idx, LEFT, 1) < len ? heights[go_0based(idx, LEFT, 1)] : 0;
  int right = go_0based(idx, RIGHT, 1) < len ? heights[go_0based(idx, RIGHT, 1)] : 0;

  return left - right;
}

int
_height(int idx, int * heights, int len)
{
  int left = go_0based(idx, LEFT, 1) < len ? heights[go_0based(idx, LEFT, 1)] : 0;
  int right = go_0based(idx, RIGHT, 1) < len ? heights[go_0based(idx, RIGHT, 1)] : 0;

  return 1 + (left > right ? left : right);
}

void
_bf_left_rotate(int idx, int * arr, int len)
{
  int a = arr[idx];
  int b = arr[go_0based(idx,RIGHT,1)];

  pull_down(go_0based(idx, LEFT, 1), LEFT, arr, len);
  shift(go_0based(go_0based(idx, RIGHT, 1), LEFT, 1), LEFT, arr, len);
  pull_up(go_0based(idx, RIGHT, 2), arr, len);

  arr[go_0based(idx, LEFT, 1)] = a;
  arr[idx]                     = b;
}

void
_bf_right_rotate(int idx, int * arr, int len)
{
  int a = arr[idx];
  int b = arr[go_0based(idx, LEFT, 1)];

  pull_down(go_0based(idx, RIGHT, 1), RIGHT, arr, len);
  shift(go_0based(go_0based(idx, LEFT, 1), RIGHT, 1), RIGHT, arr, len);
  pull_up(go_0based(idx, LEFT, 2), arr, len);

  arr[go_0based(idx, RIGHT, 1)] = a;
  arr[idx]                      = b;
}

void _left_rotate(int idx, struct tarbre * tar)
{
  _bf_left_rotate(idx, tar->values, tar->len);
  _bf_left_rotate(idx, tar->heights, tar->len);
  tar->heights[go_0based(idx,LEFT,1)] = _height(go_0based(idx,LEFT,1),tar->heights,tar->len);
  tar->heights[idx]                   = _height(idx, tar->heights,tar->len);
}

void _right_rotate(int idx, struct tarbre * tar)
{
  _bf_right_rotate(idx, tar->values, tar->len);
  _bf_right_rotate(idx, tar->heights, tar->len);
  tar->heights[go_0based(idx,RIGHT,1)] = _height(go_0based(idx,RIGHT,1),tar->heights,tar->len);
  tar->heights[idx]                    = _height(idx, tar->heights,tar->len);
}

void *
do_things (void * args)
{
  while (true) {
    // at first, we are sleeping and waiting for an impulse
    pthread_mutex_lock(&lock);
    sleeping_threads += 1;

    // no loop here, we don't care about spurious wake up because
    // we'll immediatly go back to sleep if there is nothing to do
    // anyway.
    pthread_cond_wait(&impulse, &lock);
    pthread_mutex_unlock(&lock);

    // impulse has come; it's time to work
    while (true) {
      // if we have nothing to do, go back to sleep.
      int no_more_jobs = 1;
      pthread_mutex_lock(&lock);
      no_more_jobs = jobs.empty();
      if (no_more_jobs) break;
      // if we have things to do, acquire the job.
      job my_job = jobs.front(); jobs.pop();
      nb_jobs_done += 1;
      thread_has_done[(*(int*) args)] += 1;
      pthread_mutex_unlock(&lock);

      // begin: do job
      if (my_job.dst == 0) {
        memset(my_job.arr + my_job.src, 0, my_job.sz);
      } else {
        memcpy(my_job.arr + my_job.dst, my_job.arr + my_job.src, my_job.sz);
      }
      // end: do job
    }
    // only reachable through the break, therefore we have the lock
    // back to sleep unless it's time to die.
    if (time_to_die) break;
    pthread_mutex_unlock(&lock);
  }
  // dyin'
  pthread_mutex_unlock(&lock);

  return 0;
}

void
insert(int val, struct tarbre * tar)
{
  int i = 0;
  int balance = 0;

  if (tar->len > 0) {
    /* 1. Standard BST insertion */

    /* The following loop traverses the tree to find where the next element has
     * to be inserted, `cond` is computed separately so that there is no need to
     * branch on in. */
    int * cur_value  = tar->values;
    int * cur_height = tar->heights;
    while (i < tar->len && *cur_height) {
      int cond    = val < *cur_value;
      cur_value  += (2 * (i + 1) - cond) - i;
      cur_height += (2 * (i + 1) - cond) - i;
      i           = (2 * (i + 1) - cond);
    }

    if (tar->len <= i) {
      int   new_len     = 1 <<  _greatest_bit_pos(i);
      int * new_values  = new int[new_len]();
      int * new_heights = new int[new_len]();

      
      for (int i = 0,
             *values = tar->values,
             *heights = tar->heights,
             len = tar->len; i < len ; ++i) {
        new_values[i] = values[i];
        new_heights[i] = heights[i];
      }

      delete [] tar->values;
      delete [] tar->heights;

      tar->values = new_values;
      tar->heights = new_heights;
      tar->len   = new_len;
    }

    tar->values[i] = val;
    tar->heights[i] = 1;
    tar->size++;

    // std::cout << "insert " << val << std::endl;
    // for (int v = 0 ; v < tar->len ; ++v) {
    //   std::cout << tar->heights[v] << " ";
    // }
    // std::cout << std::endl;
    /* 2. Get back to the first unbalanced node */

    do {
      i = go_0based(i, LEFT, -1);
      tar->heights[i] = _height(i, tar->heights, tar->len);
    } while (i > 0 && abs(_balance(i, tar->heights, tar->len)) < 2);

    /* 3. Rebalance */

    balance = _balance(i, tar->heights, tar->len);

    if (balance < -1) {
      bool cond = val < tar->values[go_0based(i, RIGHT, 1)];
      if (cond) {
        _right_rotate(go_0based(i, RIGHT, 1), tar);
        _left_rotate(i, tar);
      } else {
        _left_rotate(i, tar);
      }
    }

    if (balance > 1) {
      bool cond = val < tar->values[go_0based(i, LEFT, 1)];
      if (cond) {
        _right_rotate(i, tar);
      } else {
        _left_rotate(go_0based(i, LEFT, 1), tar);
        _right_rotate(i, tar);
      }
    }

    /* 4. Recompute heights */

    while (i > 0) {
      i = go_0based(i, LEFT, -1);
      tar->heights[i] = _height(i, tar->heights, tar->len);
    }

  } else {
    tar->values  = new int[1]();
    tar->heights = new int[1]();
    tar->size    = 1;
    tar->len     = 1;

    tar->values[0]  = val;
    tar->heights[0] = 1;
  }
}

int
main (int ac, char *av[])
{
  int sz = 0;
  int times;
  struct tarbre t =
    {
      .values = nullptr,
      .heights = nullptr,
      .len = 0,
      .size = 0
    };

  // opening.
  pthread_mutex_init(&lock, nullptr);
  pthread_cond_init(&impulse, nullptr);

  for (int i = 0 ; i < NB_THREADS ; ++i) {
    nb[i] = i;
  }

  for (int i = 0 ; i < NB_THREADS ; ++i) {
    pthread_create(threads + i, nullptr, do_things, nb + i);
  }

  //begin: real program
  for (int j = 1 ; j < SZ ; ++j) {
    insert(j, &t);
  }
  std::cout << t.len << std::endl;
  // for (int i = 0 ; i < t.len ; ++i) {
  //   std::cout << t.values[i] << " ";
  // }
  // std::cout << std::endl;
  // for (int i = 0 ; i < t.len ; ++i) {
  //   // std::cout << _height(i, t.heights, t.len) << " ";
  //   std::cout << t.heights[i] << " ";
  // }
  // std::cout << std::endl;
  //end: real program

  // closings.
  pthread_mutex_lock(&lock);
  time_to_die = 1;
  pthread_cond_broadcast(&impulse);
  pthread_mutex_unlock(&lock);

  for (int i = 0 ; i < NB_THREADS ; ++i) {
    pthread_join(threads[i], NULL);
  }

  // std::cout << "work units done: " << nb_jobs_done << std::endl;
  // for (int i = 0 ; i < NB_THREADS ; ++i) {
  //   std::cout
  //     << i << ": " << thread_has_done[i] << "("
  //     << (((int) (10000 * (thread_has_done[i] / ((double) nb_jobs_done)))) / ((double) 100))
  //     << ")" << std::endl;
  // }

  
  pthread_mutex_destroy(&lock);
  pthread_cond_destroy(&impulse);

  return 0;
}

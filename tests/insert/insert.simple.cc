// -*- flycheck-clang-language-standard : c++20; -*-

#include <memory>
#include <cstring>
#include <iostream>
#include <chrono>
#include <thread>
#include <vector>
#include <array>
#include <barrier>
#include <semaphore>

// #ifdef _GNU_SOURCE
// #include <sched.h>
// #endif

#define SZ (1 << 20)

// #define NB_THREADS 8

#define CHUNK_SZ 256

#define SEQ 0

#define LEFT 0
#define RIGHT 1

struct tarbre {
  int * values;
  int * heights; //todo: remove this
  int len;
  int size;
};

struct job {
  int _src, _dst, _sz, *_arr, _type;

  job()
    : _src(0), _dst(0), _sz(0), _arr(nullptr)
  {};

  job(int src, int dst, int sz, int * arr)
    : _src(src), _dst(dst), _sz(sz), _arr(arr)
  {};

};

static inline int
_greatest_bit_pos(unsigned int n)
{
  return 8*sizeof(n) - __builtin_clz(n);
}

#define max(a,b) ((a > b) ? (a) : (b))

inline int
go_1based(int base, int dir, int times)
{
  if (times < 0) {
    return max(0, base >> (- times));
  } else {
    return ((base + dir) << times) - dir;
  }
}

inline int
go_0based(int base, int dir, int times)
{
  return go_1based(base + 1, dir, times) - 1;
}

class runner {
  std::array<std::vector<job>, NB_THREADS> _jobs;
  int _tasks[NB_THREADS];
  bool _end;

  std::array<std::thread, NB_THREADS>                             _threads;
  std::array<std::unique_ptr<std::binary_semaphore>, NB_THREADS>  _sems;
  std::barrier<>                                                  _barrier;

public:
  runner()
    : _end(false),
      _barrier(NB_THREADS + 1)
  {
    for (int i = 0 ; i < NB_THREADS ; ++i) {
      _jobs[i].resize(1000000);
    }
    
    for (int i = 0 ; i < NB_THREADS ; ++i) {
      _sems[i].reset(new std::binary_semaphore(0));
    }
    for (int i = 0 ; i < NB_THREADS ; ++i) {
      _threads[i] = std::thread(&runner::f, this, i);
// #ifdef _GNU_SOURCE
//       cpu_set_t * cpu_set = CPU_ALLOC(NB_THREADS);
//       CPU_SET(i, cpu_set);
//       pthread_setaffinity_np(_threads[i].native_handle(), sizeof(cpu_set_t), cpu_set);
//       CPU_FREE(cpu_set);
// #else
// #error No thread affinity.
// #endif
    }
    for (int i = 0 ; i < NB_THREADS ; ++i) {
      _tasks[i] = 0;
    }
  }

  ~runner() {
    _end = true;
    int sum = 0;
    for (int i = 0 ; i < NB_THREADS ; ++i) {
      _sems[i]->release();
    }
    for (int i = 0 ; i < NB_THREADS ; ++i) {
      _threads[i].join();
    }
    for (int i = 0 ; i < NB_THREADS ; ++i) {
      std::cout << "thread " << i << " ran " << _tasks[i] << " tasks." << std::endl;
      sum += _tasks[i];
    }
    std::cout << "nb tasks = " << sum << std::endl;
  }
  
  void enqueue(job j, int task_set)
  {
    _jobs[task_set].push_back(j);
  }
  
  void run ()
  {
    for (int i = 0 ; i < NB_THREADS ; ++i) {
      _sems[i]->release();
    }

    _barrier.arrive_and_wait();

    for (int i = 0 ; i < NB_THREADS ; ++i) {
      _jobs[i].clear();
    }
  }

  void f (int i)
  {
    while (true) {
      _sems[i]->acquire();
      if (_end) break;

      for (auto jobs : _jobs[i]) {
        memcpy(jobs._arr + jobs._dst, jobs._arr + jobs._src, jobs._sz);
        _tasks[i]++;
      }

      _barrier.arrive_and_wait();
    }
  }
};

runner runner;

void pull_up (int idx, int * arr, int len) {
  int start_lvl = _greatest_bit_pos(idx + 1);
  int end_lvl   = _greatest_bit_pos(len - 1);
  int n         = end_lvl - start_lvl;

  /* 1. Compute the direction.
     Are we pulling-up a left sub-tree or a right sub-tree?
     0: left ; 1: right */
  int dir = (idx + 1) & 1;
  for (int i = 0 ; i <= n ; ++i) {
    int dst = go_0based(idx, dir, i - 1);
    int src = go_0based(idx, dir, i);
    arr[dst] = arr[src];
  }

  // i is the layer number plus 1.
  for (int i = 0 ; i < n ; ++i) {
    for (int j = 0 ; j < n - i ; ++j) {
      int dst = go_0based(go_0based(go_0based(idx, dir, j - 1), 1 - dir, 1), LEFT, i);
      int src = go_0based(go_0based(go_0based(idx, dir, j), 1 - dir, 1), LEFT, i);
      int size = (1 << i) * sizeof (*arr);
      // printf("write %d bytes from %d to %d\n", (1 << i), src_layer_start, dst_layer_start);
      // 4.1.1 Create copy jobs of size `chunk`.

      #if SEQ == 1
      memcpy(arr + dst, arr + src, size);
      #else
      job job(src, dst, size, arr);
      runner.enqueue(job, i % NB_THREADS);
      #endif

      // std::cout << "sleeping threads: " << sleeping_threads << std::endl;
    }
  }

  #if SEQ == 0
  runner.run();
  #endif

  // Clean up, memset 0 deepest level below (idx + 1) / 2 - 1
  /* printf("clear %d with %d zeroes\n", */
  /*     (1 << (end_lvl - start_lvl + 1)) * ((idx + 1) / 2) - 1, */
  /*     (1 << (end_lvl - start_lvl))); */
  // memset(arr + (1 << (end_lvl - start_lvl + 1)) * ((idx + 1) / 2) - 1,
  //       0,
  //       (1 << (end_lvl - start_lvl + 1)) * (sizeof *arr));
  memset(arr + (1 << (end_lvl - start_lvl + 1)) * ((idx + 1) / 2) - 1,
           0,
           (1 << (end_lvl - start_lvl + 1)) * (sizeof *arr));
}

void
pull_down(int idx, int dir, int * arr, int len)
{
      int start_lvl = _greatest_bit_pos(idx + 1);
      int end_lvl   = _greatest_bit_pos(len - 1);
      int n = end_lvl - start_lvl - 1;

      // printf("dir = %d\n", dir);
      // printf("sl: %d ; el: %d\n", start_lvl, end_lvl);
      for (int i = n ; i >= 0 ; --i) {
        int src = go_0based(idx, dir, i);
        int dst = go_0based(idx, dir, i + 1);
	// printf("arr[%d] = array[%d]\n", dst, src);
	arr[dst] = arr[src];
      }
      // i is the layer number plus 1.
      for (int i = 0 ; i < n ; ++i) {
        // j walks the layer labeled layer number.
        for (int j = 0 ; j < n - i; j++) {
          int src = (1 << i) * (2 * ((idx + dir + 1) * (1 << (n - (i + j + 1))) - dir) + (1 - dir)) - 1;
          int dst = (1 << i) * (2 * ((idx + dir + 1) * (1 << (n - (i + j)))     - dir) + (1 - dir)) - 1;
          int size = (1 << i) * sizeof (*arr);

          #if SEQ == 1
          memcpy(arr + dst, arr + src, size);
          #else
          job job(src, dst, size, arr);
          runner.enqueue(job, i % NB_THREADS);
          #endif
        }
      }

      #if SEQ == 0
      runner.run();
      #endif

      /* Clean up */
      arr[(idx + 1) - 1] = 0;
      for (int i = 0 ; i < end_lvl - start_lvl ; ++i) {
        int depth = 1 << i;
        int size  = depth * (sizeof *arr);
        int dest  = depth * (2 * (idx + 1) + (1 - dir)) - 1;

        memset(arr + dest, 0, size);
      }
}

void
shift(int idx, int dir, int * arr, int len) {
  int start_lvl = _greatest_bit_pos(idx + 1);
  int end_lvl   = _greatest_bit_pos(len - 1);
  int steps     = end_lvl - start_lvl + 1;
  int dir_coef  = 2 * dir - 1; /* 0 -> -1 ; 1 -> 1 */

  for (int i = 0 ; i < steps ; ++i) {
    int depth = 1 << i;
    int size  = depth * (sizeof *arr);
    int src   = depth * (idx + 1) - 1;
    int dst   = src + dir_coef * depth;

    #if SEQ == 1
    memcpy(arr + dst, arr + src, size);
    #else
    job job(src, dst, size, arr);
    runner.enqueue(job, i % NB_THREADS);
    #endif
  }

  #if SEQ == 0
  runner.run();
  #endif
}

int
_balance(int idx, int * heights, int len)
{
  int left = go_0based(idx, LEFT, 1) < len ? heights[go_0based(idx, LEFT, 1)] : 0;
  int right = go_0based(idx, RIGHT, 1) < len ? heights[go_0based(idx, RIGHT, 1)] : 0;

  return left - right;
}

int
_height(int idx, int * heights, int len)
{
  int left = go_0based(idx, LEFT, 1) < len ? heights[go_0based(idx, LEFT, 1)] : 0;
  int right = go_0based(idx, RIGHT, 1) < len ? heights[go_0based(idx, RIGHT, 1)] : 0;

  return 1 + (left > right ? left : right);
}

void
_bf_left_rotate(int idx, int * arr, int len)
{
  int a = arr[idx];
  int b = arr[go_0based(idx,RIGHT,1)];

  pull_down(go_0based(idx, LEFT, 1), LEFT, arr, len);
  shift(go_0based(go_0based(idx, RIGHT, 1), LEFT, 1), LEFT, arr, len);
  pull_up(go_0based(idx, RIGHT, 2), arr, len);

  arr[go_0based(idx, LEFT, 1)] = a;
  arr[idx]                     = b;
}

void
_bf_right_rotate(int idx, int * arr, int len)
{
  int a = arr[idx];
  int b = arr[go_0based(idx, LEFT, 1)];

  pull_down(go_0based(idx, RIGHT, 1), RIGHT, arr, len);
  shift(go_0based(go_0based(idx, LEFT, 1), RIGHT, 1), RIGHT, arr, len);
  pull_up(go_0based(idx, LEFT, 2), arr, len);

  arr[go_0based(idx, RIGHT, 1)] = a;
  arr[idx]                      = b;
}

void _left_rotate(int idx, struct tarbre * tar)
{
  _bf_left_rotate(idx, tar->values, tar->len);
  _bf_left_rotate(idx, tar->heights, tar->len);
  tar->heights[go_0based(idx,LEFT,1)] = _height(go_0based(idx,LEFT,1),tar->heights,tar->len);
  tar->heights[idx]                   = _height(idx, tar->heights,tar->len);
}

void _right_rotate(int idx, struct tarbre * tar)
{
  _bf_right_rotate(idx, tar->values, tar->len);
  _bf_right_rotate(idx, tar->heights, tar->len);
  tar->heights[go_0based(idx,RIGHT,1)] = _height(go_0based(idx,RIGHT,1),tar->heights,tar->len);
  tar->heights[idx]                    = _height(idx, tar->heights,tar->len);
}

void
insert(int val, struct tarbre * tar)
{
  int i = 0;
  int balance = 0;

  if (tar->len > 0) {
    /* 1. Standard BST insertion */

    /* The following loop traverses the tree to find where the next element has
     * to be inserted, `cond` is computed separately so that there is no need to
     * branch on in. */
    int * cur_value  = tar->values;
    int * cur_height = tar->heights;
    while (i < tar->len && *cur_height) {
      int cond    = val < *cur_value;
      cur_value  += (2 * (i + 1) - cond) - i;
      cur_height += (2 * (i + 1) - cond) - i;
      i           = (2 * (i + 1) - cond);
    }

    if (tar->len <= i) {
      int   new_len     = 1 <<  _greatest_bit_pos(i);
      int * new_values  = new int[new_len]();
      int * new_heights = new int[new_len]();

      
      for (int i = 0,
             *values = tar->values,
             *heights = tar->heights,
             len = tar->len; i < len ; ++i) {
        new_values[i] = values[i];
        new_heights[i] = heights[i];
      }

      delete [] tar->values;
      delete [] tar->heights;

      tar->values = new_values;
      tar->heights = new_heights;
      tar->len   = new_len;
    }

    tar->values[i] = val;
    tar->heights[i] = 1;
    tar->size++;

    // std::cout << "insert " << val << std::endl;
    // for (int v = 0 ; v < tar->len ; ++v) {
    //   std::cout << tar->heights[v] << " ";
    // }
    // std::cout << std::endl;
    /* 2. Get back to the first unbalanced node */

    do {
      i = go_0based(i, LEFT, -1);
      tar->heights[i] = _height(i, tar->heights, tar->len);
    } while (i > 0 && abs(_balance(i, tar->heights, tar->len)) < 2);

    /* 3. Rebalance */

    balance = _balance(i, tar->heights, tar->len);

    if (balance < -1) {
      bool cond = val < tar->values[go_0based(i, RIGHT, 1)];
      if (cond) {
        _right_rotate(go_0based(i, RIGHT, 1), tar);
        _left_rotate(i, tar);
      } else {
        _left_rotate(i, tar);
      }
    }

    if (balance > 1) {
      bool cond = val < tar->values[go_0based(i, LEFT, 1)];
      if (cond) {
        _right_rotate(i, tar);
      } else {
        _left_rotate(go_0based(i, LEFT, 1), tar);
        _right_rotate(i, tar);
      }
    }

    /* 4. Recompute heights */

    while (i > 0) {
      i = go_0based(i, LEFT, -1);
      tar->heights[i] = _height(i, tar->heights, tar->len);
    }

  } else {
    tar->values  = new int[1]();
    tar->heights = new int[1]();
    tar->size    = 1;
    tar->len     = 1;

    tar->values[0]  = val;
    tar->heights[0] = 1;
  }
}

int
main ()
{

  struct tarbre t =
    {
      .values = nullptr,
      .heights = nullptr,
      .len = 0,
      .size = 0
    };

  for (int i = 1 ; i < SZ ; ++i)
    insert(i, &t);
  
  // for (int i = 0 ; i < t.len ; ++i) {
  //   std::cout << t.values[i] << " ";
  // }
  // std::cout << std::endl;

  return 0;
}

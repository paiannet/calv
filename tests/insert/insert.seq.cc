#include <iostream>
#include <sstream>
#include <cstring>
#include <vector>
#include <map>
#include <chrono>
#include "avl-tree.h"
#include "avl-bf.h"

using namespace bbt;
#define SZ (1 << 10)

int
main () {

  avl_bf<int> avl;
  for (int i = 1 ; i < SZ ; ++i) {
    avl.insert(i);
  }
  std::cout << avl.density() << std::endl;

  return 0;
}

#include <iostream>
#include <sstream>
#include <cstring>
#include <vector>
#include <map>
#include <chrono>
#include "avl-tree.h"
#include "avl-bf.h"

using namespace bbt;

int
main () {

  my_tree<int> avl;
  std::string cur_line, token;
  std::string cur_op;
  std::chrono::steady_clock::time_point begin, end;
  int w = 0, d = 0, g = 0;

  begin = std::chrono::steady_clock::now();
  while (std::getline(std::cin, cur_line)) {
    std::istringstream iss(cur_line);
    while (std::getline(std::cin, cur_line)) {
      std::istringstream iss(cur_line);
      std::string op;
      std::string val;

      iss >> op;
      iss >> val;
      if (op == "Write") {
        avl.insert(std::stoi(val));
        w++;
      } else if (op == "Delete") {
        avl.find(std::stoi(val));
        d++;
      } else if (op == "Get") {
        avl.find(std::stoi(val));
        g++;
      } else if (op[0] == '-' && op[1] == '-' && op[2] != '-') {
        /* std::cout << "w: " << w << " g: " << g << " d: " << d << std::endl; */
        end = std::chrono::steady_clock::now();
        std::cout << "Time difference (" << cur_op << ") = " << std::chrono::duration_cast<std::chrono::microseconds>((end - begin)).count() << "[us]" << std::endl;
        begin = std::chrono::steady_clock::now();
        cur_op = op.substr(2);
      } else {
        /* Unsupported operation */
      }
    }
  }
  end = std::chrono::steady_clock::now();
  std::cout << "Time difference (" << cur_op << ") = " << std::chrono::duration_cast<std::chrono::microseconds>((end - begin)).count() << "[us]" << std::endl;

  return 0;
}

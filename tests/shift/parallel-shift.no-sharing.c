#include <omp.h>
#include <string.h>
#include <stdio.h>

#define TIMES 100000
#define SZ 18
#define NB_THREAD 9

int array[1 << (SZ + 1)];

static inline int
_greatest_bit_pos(unsigned int n) {
  return 8*sizeof(n) - __builtin_clz(n);
}


int
main (void)
{

#pragma omp parallel for num_threads(NB_THREAD)
  for (int i = 0; i < (1 << SZ); ++i) {
    array[i] = i + 1;
  }

for (int t = 0 ; t < TIMES ; ++t) {
  int idx = 1;
  int len_ = 1 << (SZ + 1);
  int dir = 1;
  int start_lvl = _greatest_bit_pos(idx + 1);
  int end_lvl   = _greatest_bit_pos(len_ - 1);
  int steps     = end_lvl - start_lvl + 1;
  int dir_coef  = 2 * dir - 1; /* 0 -> -1 ; 1 -> 1 */

#pragma omp parallel for simd num_threads(NB_THREAD)
  for (int i = 0 ; i < steps ; ++i) {
    int depth = 1 << i;
    int size  = depth * (sizeof *array);
    int src   = depth * (idx + 1) - 1;
    int dest  = src + dir_coef * depth;

    memcpy(array + dest, array + src, size);
    memset(array + src, 0, size);
  }

  /* for (int i = (1 << SZ)*0 ; i < (1 << (SZ + 1)) ; ++i) { */
  /*   printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : ""); */
  /* } */
  /* putchar('\n'); */
}

  return 0;
}

#include <cstring>
#include <chrono>
#include <iostream>
#include <omp.h>

#define NB_THREAD 9

static inline int
_greatest_bit_pos(unsigned int n) {
  return 8*sizeof(n) - __builtin_clz(n);
}

int
main (int ac, char *av[])
{
  int sz;
  int times;
 
  if (ac == 3
      && (sz = std::strtol(av[1], NULL, 10))
      && (times = std::strtol(av[2], NULL, 10))) {
    int * array = new int[1 << (sz + 1)];
    /* Setting the pragma here makes it faster, because threads can be reused. */
    for (int i = 0; i < (1 << sz); ++i) {
      array[i] = i + 1;
    }

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
#pragma omp parallel num_threads(NB_THREAD)
    {

      for (int t = 0 ; t < times ; ++t) {
        int idx = 1;
        int len_ = 1 << (sz + 1);
        int dir = 1;
        int start_lvl = _greatest_bit_pos(idx + 1);
        int end_lvl   = _greatest_bit_pos(len_ - 1);
        int steps     = end_lvl - start_lvl + 1;
        int dir_coef  = 2 * dir - 1; /* 0 -> -1 ; 1 -> 1 */

#pragma omp for simd
        for (int i = 0 ; i < steps ; ++i) {
          int depth = 1 << i;
          int size  = depth * (sizeof *array);
          int src   = depth * (idx + 1) - 1;
          int dest  = src + dir_coef * depth;

          memcpy(array + dest, array + src, size);
          memset(array + src, 0, size);
        }

        /* for (int i = (1 << sz)*0 ; i < (1 << (SZ + 1)) ; ++i) { */
        /*   printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : ""); */
        /* } */
        /* putchar('\n'); */
      }
    }
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Time difference (shift) = " << std::chrono::duration_cast<std::chrono::microseconds>((end - begin) / times).count() << "[µs]" << std::endl;
    delete [] array;
  }
  return 0;
}

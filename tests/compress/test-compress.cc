#include <iostream>
#include <sstream>
#include <cstring>
#include <vector>
#include <map>

#include <avl-bf.h>
#include <str.h>

using namespace bbt;

int
main () {

#define my_tree avl_bf
  std::vector<int> a = {2, 1, 4, 3};
  avl_bf<int> avl;

  for (auto& i: a) {
    avl.insert(i);
  }
  avl.compress();
  std::cout << avl;

  return 0;
}

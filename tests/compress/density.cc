#include <iostream>
#include <cstring>
#include <ctime>
#include <chrono>

#include "avl-bf.h"

#define TIMES 5

using namespace bbt;

int
main () {
  srand(time(NULL));
  for (int j = 0 ; j < 23 ; ++j) {
    int sz = (1 << j) - 1;
    std::vector<int> a(sz);
    for (int i = 0 ; i < sz ; ++i) {
      a[i] = i + 1;
    }

    for (int i = 0 ; i < sz ; ++i) {
      int idx = static_cast<int>((rand() / static_cast<double>(RAND_MAX)) * (sz - i));
      int tmp = a[sz - 1 - i];
      a[sz - 1 - i] = a[idx];
      a[idx] = tmp;
    }

    std::cout << j;
    std::cerr << j;
    for (int i = 0 ; i < 40 ; i++) {
      float compress_threshold = i / static_cast<double>(100);
      std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
      for (int times = 0 ; times < TIMES ; ++times) {
        avl_bf<int> tree(compress_threshold);

        for (int i = 0 ; i < sz ; ++i) {
          tree.insert(a[i]);
        }
      }
      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

      double time_diff = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() / (TIMES * static_cast<double>(1000000));
      std::cout << "," << time_diff;
    }
    std::cout << std::endl;
  }
}

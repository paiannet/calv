#include <iostream>
#include <sstream>
#include <cstring>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <functional>
#include <chrono>

#include <avl-tree.h>
#include <avl-bf.h>
#include <str.h>

using namespace bbt;

int
square (int i)
{
  return i+1;
}

int
main (int ac, char *av[]) {

  int nb;
  int times;

  if (ac == 4
     && (nb = 1 << std::strtol(av[2], NULL, 10))
     && (times = std::strtol(av[3], NULL, 10))
     && !strcmp(av[1], "avl-bf")) {
    avl_bf<int> avl;

    /* std::chrono::steady_clock::time_point begini = std::chrono::steady_clock::now(); */
    for (int i = 1 ; i < nb ; i++)
      avl.insert(i);
    /* std::chrono::steady_clock::time_point endi = std::chrono::steady_clock::now(); */
    /* std::cout << "Time difference (insert) = " << std::chrono::duration_cast<std::chrono::microseconds>((endi - begini) / times).count() << "[µs]" << std::endl; */

    std::chrono::steady_clock::time_point beginm = std::chrono::steady_clock::now();
    for (int i = 0 ; i < times ; i++)
      avl.map(square);
    std::chrono::steady_clock::time_point endm = std::chrono::steady_clock::now();
    std::cout << "Time difference (map) = " << std::chrono::duration_cast<std::chrono::microseconds>((endm - beginm) / times).count() << "[µs]" << std::endl;

  } else if (ac == 4
     && (nb = 1 << std::strtol(av[2], NULL, 10))
     && (times = std::strtol(av[3], NULL, 10))
     && !strcmp(av[1], "avl-tree")) {
    avl_tree<int> avl;

    /* std::chrono::steady_clock::time_point begini = std::chrono::steady_clock::now(); */
    for (int i = 1 ; i < nb ; i++)
      avl.insert(i);
    /* std::chrono::steady_clock::time_point endi = std::chrono::steady_clock::now(); */
    /* std::cout << "Time difference (insert) = " << std::chrono::duration_cast<std::chrono::microseconds>((endi - begini) / times).count() << "[µs]" << std::endl; */

    std::chrono::steady_clock::time_point beginm = std::chrono::steady_clock::now();
    for (int i = 0 ; i < times ; i++)
      avl.map(square);
    std::chrono::steady_clock::time_point endm = std::chrono::steady_clock::now();
    std::cout << "Time difference (map) = " << std::chrono::duration_cast<std::chrono::microseconds>((endm - beginm) / times).count() << "[µs]" << std::endl;
  } else if (ac == 4
     && (nb = 1 << std::strtol(av[2], NULL, 10))
     && (times = std::strtol(av[3], NULL, 10))
     && !strcmp(av[1], "set")) {
    std::map<int, int> avl;

    /* std::chrono::steady_clock::time_point begini = std::chrono::steady_clock::now(); */
    for (int i = 1 ; i < nb ; i++)
      avl.insert( std::pair<int,int>(i,i));
    /* std::chrono::steady_clock::time_point endi = std::chrono::steady_clock::now(); */
    /* std::cout << "Time difference (insert) = " << std::chrono::duration_cast<std::chrono::microseconds>((endi - begini) / times).count() << "[µs]" << std::endl; */

    std::chrono::steady_clock::time_point beginm = std::chrono::steady_clock::now();
    for (int i = 0 ; i < times ; i++) {
      for (auto it = avl.begin() ; it != avl.end() ; ++it) {
	it->second = square(it->second);
      }
    }
    std::chrono::steady_clock::time_point endm = std::chrono::steady_clock::now();
    std::cout << "Time difference (map) = " << std::chrono::duration_cast<std::chrono::microseconds>((endm - beginm) / times).count() << "[µs]" << std::endl;
  } else {
    std::cout << av[0] << " [avl-tree|avl-bf|set] nb times" << std::endl;
  }



  return 0;
}

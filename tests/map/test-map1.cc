#include <iostream>
#include <sstream>
#include <cstring>
#include <vector>
#include <map>
#include <functional>

#include <str.h>
#include <avl-tree.h>
#include <avl-bf.h>

using namespace bbt;

str
do_things (str s)
{
  char ** ps = s.get();
  if (ps && *ps) {
    **ps += 1;
  }

  return s;
}

int
main () {

#define my_tree avl_bf
  avl_bf<str> avl;
  std::function<str(str)> f = do_things;

  avl.insert(str("hello"));
  avl.insert(str("world"));
  avl.insert(str("the"));
  avl.insert(str("sun"));
  avl.insert(str("is"));
  avl.insert(str("bright"));
  std::cout << avl;
  avl.map(f);
  std::cout << avl;

  return 0;
}

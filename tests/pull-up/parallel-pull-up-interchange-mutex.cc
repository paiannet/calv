#include <cstring>
#include <iostream>
#include <chrono>
#include <queue>

#include <pthread.h>

using time_unit = std::chrono::microseconds;

#define LEFT 0
#define RIGHT 1

#define NB_TIMES 1000
#define BLOCK_STEP (1 << 10)

struct job {
  int src, dst, sz;
  job()
    : src(0), dst(0), sz(0)
  {};
  job(int src_, int dst_, int sz_)
    : src(src_), dst(dst_), sz(sz_)
  {};
};

pthread_cond_t impulse;
pthread_mutex_t lock;

int nb_jobs_done = 0;
int time_to_die = 0;
int sleeping_threads = 0;

pthread_t threads[NB_THREADS];
int thread_has_done[NB_THREADS] = {0};
int nb[NB_THREADS] = {0};

std::queue<job> jobs;

int * array;

void *
do_things (void * args)
{
  while (true) {
    // at first, we are sleeping and waiting for an impulse
    pthread_mutex_lock(&lock);
    sleeping_threads += 1;

    // no loop here, we don't care about spurious wake up because
    // we'll immediatly go back to sleep if there is nothing to do
    // anyway.
    pthread_cond_wait(&impulse, &lock);
    pthread_mutex_unlock(&lock);

    // impulse has come; it's time to work
    while (true) {
      // if we have nothing to do, go back to sleep.
      int no_more_jobs = 1;
      pthread_mutex_lock(&lock);
      no_more_jobs = jobs.empty();
      if (no_more_jobs) break;
      // if we have things to do, acquire the job.
      job my_job = jobs.front(); jobs.pop();
      nb_jobs_done += 1;
      thread_has_done[(*(int*) args)] += 1;
      pthread_mutex_unlock(&lock);

      // begin: do job
      if (my_job.dst == 0) {
        memset(array + my_job.src, 0, my_job.sz);
      } else {
        memcpy(array + my_job.dst, array + my_job.src, my_job.sz);
      }
      // end: do job
    }
    // only reachable through the break, therefore we have the lock
    // back to sleep unless it's time to die.
    if (time_to_die) break;
    pthread_mutex_unlock(&lock);
  }
  // dyin'
  pthread_mutex_unlock(&lock);

  return 0;
}

static inline int
_greatest_bit_pos(unsigned int n)
{
  return 8*sizeof(n) - __builtin_clz(n);
}

#define max(a,b) ((a > b) ? (a) : (b))

inline int
go_1based(int base, int dir, int times)
{
  if (times < 0) {
    return max(0, base >> (- times));
  } else {
    return ((base + dir) << times) - dir;
  }
}

inline int
go_0based(int base, int dir, int times)
{
  return go_1based(base + 1, dir, times) - 1;
}

int
main (int ac, char *av[])
{
  int sz = 0;
  int times;
  int chunk = (1 << 30);

  // opening.
  pthread_mutex_init(&lock, nullptr);
  pthread_cond_init(&impulse, nullptr);

  for (int i = 0 ; i < NB_THREADS ; ++i) {
    nb[i] = i;
  }

  for (int i = 0 ; i < NB_THREADS ; ++i) {
    pthread_create(threads + i, nullptr, do_things, nb + i);
  }

  //begin: real program
  if (ac == 3
      && (sz = std::strtol(av[1], NULL, 10))
      && (times = std::strtol(av[2], NULL, 10))) {

    // 1. initialize array
    // no need to fill it, what we want to measure is how fast we can move
    // values around
    array = new int[(1 << (sz + 1)) - 1];
    for (long i = 0; i < (1 << (sz + 1)) - 1 ; ++i) {
      array[i] = i;
    }

    // for (int i = (1 << sz)*0 ; i < (1 << (sz + 1)) - 1 ; ++i) {
    //   printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : "");
    // }
    // putchar('\n');

    for (int b = BLOCK_STEP ; b < (1 << sz) ; b += BLOCK_STEP) {
      std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
      for (int t = 0 ; t < times ; ++t) {
        int idx = 2;
        int len = 1 << (sz + 1);
        int start_lvl = _greatest_bit_pos(idx + 1);
        int end_lvl   = _greatest_bit_pos(len - 1);
        int n = end_lvl - start_lvl;
        // printf("sl: %d ; el: %d\n", start_lvl, end_lvl);

        /* 1. Compute the direction.
           Are we pulling-up a left sub-tree or a right sub-tree?
           0: left ; 1: right */
        int dir = (idx + 1) & 1;
        for (int i = 0 ; i <= n ; ++i) {
          int dst = go_0based(idx, dir, i - 1);
          int src = go_0based(idx, dir, i);
          // printf("array[%d] = array[%d] (%d <- %d)\n", dst, src, array[dst], array[src]);
          array[dst] = array[src];
        }

        // i is the layer number plus 1.
        for (int i = 0 ; i < n ; ++i) {
          for (int j = 0 ; j < n - i ; ++j) {
            int dst = go_0based(go_0based(go_0based(idx, dir, j - 1), 1 - dir, 1), LEFT, i);
            int src = go_0based(go_0based(go_0based(idx, dir, j), 1 - dir, 1), LEFT, i);
            int size = (1 << i) * sizeof (*array);
            // printf("write %d bytes from %d to %d\n", (1 << i), src_layer_start, dst_layer_start);
            // 4.1.1 Create copy jobs of size `chunk`.
            for (int k = 0 ; k < size / chunk ; ++k) {
              // fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
              //   src + j * chunk / (sizeof *array),
              //   dst + j * chunk / (sizeof * array),
              //   chunk);
              job job(src + k * chunk / (sizeof * array),
                      dst + k * chunk / (sizeof * array),
                      chunk);

              pthread_mutex_lock(&lock);
              jobs.push(job);
              // std::cout << "sleeping threads: " << sleeping_threads << std::endl;

              if (sleeping_threads > 0) {
                // std::cout << "sending impulse" << std::endl;
                pthread_cond_signal(&impulse);
                sleeping_threads -= 1;
              }
              pthread_mutex_unlock(&lock);
            }
            // 4.1.2 Create a copy job for the leftover
            int leftover = (size / chunk) * chunk;
            job job(src  + leftover / (sizeof * array),
                    dst  + leftover / (sizeof * array),
                    size - leftover);
            /* fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n", */
            /*   src + leftover / (sizeof * array), */
            /*   dst + leftover / (sizeof * array), */
            /*   size - leftover); */
            pthread_mutex_lock(&lock);
            jobs.push(job);

            if (sleeping_threads > 0) {
              pthread_cond_signal(&impulse);
              sleeping_threads -= 1;
            }
            pthread_mutex_unlock(&lock);
          }
        }

        // 6. scheduling point:
        // wait for all jobs to be done before cleaning.
        while (true) {
          int no_more_jobs;
          pthread_mutex_lock(&lock);
          no_more_jobs = jobs.empty();
          pthread_mutex_unlock(&lock);
          if (no_more_jobs) break;
        }

        // Clean up, memset 0 deepest level below (idx + 1) / 2 - 1
        /* printf("clear %d with %d zeroes\n", */
        /*     (1 << (end_lvl - start_lvl + 1)) * ((idx + 1) / 2) - 1, */
        /*     (1 << (end_lvl - start_lvl))); */
        memset(
               array + (1 << (end_lvl - start_lvl + 1)) * ((idx + 1) / 2) - 1,
               0,
               (1 << (end_lvl - start_lvl + 1)) * (sizeof *array)
               );

      }

      // for (int i = (1 << sz)*0 ; i < (1 << (sz + 1)) - 1 ; ++i) {
      //   printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : "");
      // }
      // putchar('\n');
      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
      // block_size, nb_threads, duration
      std::cout
        << b << ", " // block_size
        << NB_THREADS << ", " // nb_threads
        << std::chrono::duration_cast<time_unit>((end - begin) / times).count() // duration
        << std::endl;
    }
    delete [] array;
  }
  //end: real program

  // closings.
  pthread_mutex_lock(&lock);
  time_to_die = 1;
  pthread_cond_broadcast(&impulse);
  pthread_mutex_unlock(&lock);

  for (int i = 0 ; i < NB_THREADS ; ++i) {
    pthread_join(threads[i], NULL);
  }

  // std::cout << "work units done: " << nb_jobs_done << std::endl;
  // for (int i = 0 ; i < NB_THREADS ; ++i) {
  //   std::cout
  //     << i << ": " << thread_has_done[i] << "("
  //     << (((int) (10000 * (thread_has_done[i] / ((double) nb_jobs_done)))) / ((double) 100))
  //     << ")" << std::endl;
  // }

  pthread_mutex_destroy(&lock);
  pthread_cond_destroy(&impulse);

  return 0;
}

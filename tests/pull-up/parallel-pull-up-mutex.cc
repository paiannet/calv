#include <pthread.h>
#include <iostream>
#include <cstdio>
#include <chrono>
#include <cstdlib>
#include <unistd.h>
#include <queue>
#include <cstring>

#define LEFT 0
#define RIGHT 1

using time_unit = std::chrono::microseconds;

#define max(a,b) ((a > b) ? (a) : (b))

inline int
go_1based(int base, int dir, int times) {
  if (times < 0) {
    return max(0, base >> (- times));
  } else {
    return ((base + dir) << times) - dir;
  }
}

inline int
go_0based(int base, int dir, int times) {
  return go_1based(base + 1, dir, times) - 1;
}

int * array;

struct job {
  int src, dst, sz;
  job()
    : src(0), dst(0), sz(0)
  {};
  job(int src_, int dst_, int sz_)
    : src(src_), dst(dst_), sz(sz_)
  {};
};

pthread_mutex_t mutex;
pthread_barrier_t barrier;
pthread_t threads[NB_THREADS];
std::queue<job> queue;
int end = 0;
int chunk = 4096*4096;

static inline int
_greatest_bit_pos(unsigned int n) {
  return 8*sizeof(n) - __builtin_clz(n);
}

void*
do_things (void* args) {

  int round = 1;
  while (true) {
    job my_job;
    // fprintf(stderr, "thread %d round %d\n", *(int *) args, round);
    // fprintf(stderr, "thread %d trying to acquire lock\n", *(int *) args);
    pthread_mutex_lock(&mutex);
    if (end) {
      pthread_mutex_unlock(&mutex);
      break;
    } else {
      // fprintf(stderr, "thread %d got the lock\n", *(int *) args);
      if (queue.empty()) {
        // fprintf(stderr, "thread %d: nothing to do\n", *(int *) args);
        pthread_mutex_unlock(&mutex);
        pthread_barrier_wait(&barrier);
        continue;
      } else {
        my_job = queue.front(); queue.pop();
        // fprintf(stderr,
        //   "thread %d acquire copy job: src: %d, dst: %d, sz: %d\n",
        //   *(int *) args, my_job.src, my_job.dst, my_job.sz);
      }
      // fprintf(stderr, "thread %d release the lock\n", *(int *) args);
      pthread_mutex_unlock(&mutex);
    }

    // Do the job
    if (my_job.dst == 0) {
      memset(array + my_job.src, 0, my_job.sz);
    } else {
      memcpy(array + my_job.dst, array + my_job.src, my_job.sz);
    }


    // fprintf(stderr, "thread %d call wait\n", *(int *) args);
    pthread_barrier_wait(&barrier);
    // fprintf(stderr, "thread %d over the barrier\n", *(int *) args);
    round++;
  }

  // fprintf(stderr, "thread %d terminates\n", *(int *) args);
  pthread_barrier_wait(&barrier);
  return 0;
}

int
main (int ac, char * av[])
{
  int sz;
  int times;

  if (ac == 3
      && (sz = std::strtol(av[1], NULL, 10))
      && (times = std::strtol(av[2], NULL, 10))) {

    // 1. initialize array
    // no need to fill it, what we want to measure is how fast we can move
    // values around
    array = new int[(1 << (sz + 1)) - 2];
    for (long i = 0; i < (1 << (sz + 1)) - 2 ; ++i) {
      array[i] = i;
    }
    /* puts("before pull up"); */
    /* for (int i = (1 << sz)*0 ; i < (1 << (sz + 1)) - 2 ; ++i) { */
    /*     printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : ""); */
    /* } */
    /* putchar('\n'); */

    // 2. Create the thread pool
    pthread_mutex_init(&mutex, NULL);
    pthread_barrier_init(&barrier, NULL, NB_THREADS + 1);

    for (int i = 0 ; i < NB_THREADS ; i++) {
      pthread_create(threads + i, NULL, do_things, NULL);
      pthread_detach(threads[i]);
    }


    auto begin = std::chrono::steady_clock::now();
    for (int t = 0 ; t < times ; ++t) {
      /* fprintf(stderr, "time %d\n", t); */
      // 3. initialize the pull-up parameters
      int idx = 1;
      int len = 1 << (sz + 1);
      int start_lvl = _greatest_bit_pos(idx + 1);
      int end_lvl   = _greatest_bit_pos(len - 1);
      int n = end_lvl - start_lvl;
      int dir = (idx + 1) & 1;

      // 4. move a sub-tree
      for (int k = 0 ; k < n ; k++) {
        // 4.1 move a layer
        for (int i = 0 ; i < n - k ; ++i) {
          int depth = 1 << i;
          int size  = depth * (sizeof *array);
          int dst = go_0based(go_0based(go_0based(idx, dir, k - 1), 1 - dir, 1), LEFT, i);
          int src = go_0based(go_0based(go_0based(idx, dir, k), 1 - dir, 1), LEFT, i);

          // 4.1.1 Create copy jobs of size `chunk`.
          for (int j = 0 ; j < size / chunk ; ++j) {
            // fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
            //   src + j * chunk / (sizeof *array),
            //   dst + j * chunk / (sizeof * array),
            //   chunk);
            job job(src + j * chunk / (sizeof * array),
                    dst + j * chunk / (sizeof * array),
                    chunk);
            pthread_mutex_lock(&mutex);
            queue.push(job);
            pthread_mutex_unlock(&mutex);
          }

          // 4.1.2 Create a copy job for the leftover
          int leftover = (size / chunk) * chunk;
          job job(src  + leftover / (sizeof * array),
                  dst  + leftover / (sizeof * array),
                  size - leftover);
          /* fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n", */
          /*   src + leftover / (sizeof * array), */
          /*   dst + leftover / (sizeof * array), */
          /*   size - leftover); */
          pthread_mutex_lock(&mutex);
          queue.push(job);
          pthread_mutex_unlock(&mutex);
        }
        /* putchar('\n'); */

        // 4.2 scheduling point:
        // wait for all jobs to be done.
        while (true) {
          pthread_mutex_lock(&mutex);
          if (queue.empty()) {
            pthread_mutex_unlock(&mutex);
            // fprintf(stderr, "no more jobs\n");
            break;
          } else {
            pthread_mutex_unlock(&mutex);
            pthread_barrier_wait(&barrier);
          }
        }

        array[go_0based(idx, dir, k - 1)] = array[go_0based(idx, dir, k)];
      }

      // 5. Clean up
      // todo: check bounds
      {
        int size = (1 << (end_lvl - start_lvl + 1)) * (sizeof *array);
        int dst  = (1 << (end_lvl - start_lvl + 1)) * ((idx + 1) / 2) - 1;

        // 5.1 Create copy jobs of size `chunk`.
        for (int j = 0 ; j < size / chunk ; ++j) {
          // fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
          //   src + j * chunk / (sizeof *array),
          //   dst + j * chunk / (sizeof * array),
          //   chunk);
          job job(dst + j * chunk / (sizeof * array), 0, chunk);
          pthread_mutex_lock(&mutex);
          queue.push(job);
          pthread_mutex_unlock(&mutex);
        }

        // 5.2 Create a copy job for the leftover
        int leftover = (size / chunk) * chunk;
        job job(dst + leftover / (sizeof * array), 0, size - leftover);
        // fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
        //   src + leftover / (sizeof * array),
        //   dst + leftover / (sizeof * array),
        //   size - leftover);
        pthread_mutex_lock(&mutex);
        queue.push(job);
        pthread_mutex_unlock(&mutex);
      }

      // 6. scheduling point:
      // wait for all clean jobs to be done.
      while (true) {
        pthread_mutex_lock(&mutex);
        if (queue.empty()) {
          pthread_mutex_unlock(&mutex);
          // fprintf(stderr, "no more jobs\n");
          break;
        } else {
          pthread_mutex_unlock(&mutex);
          pthread_barrier_wait(&barrier);
        }
      }
    }
    auto endp = std::chrono::steady_clock::now();
    std::cout
      << std::chrono::duration_cast<time_unit>((endp - begin) / times).count()
      << std::endl;

    /* puts("after pull up"); */
    /* for (int i = (1 << sz)*0 ; i < (1 << (sz + 1)) - 1 ; ++i) { */
    /*   printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : ""); */
    /* } */
    /* putchar('\n'); */

    // 7. delete array
    delete [] array;

    // 8. conclude the program
    pthread_mutex_lock(&mutex);
    end = 1;
    pthread_mutex_unlock(&mutex);
    pthread_barrier_wait(&barrier);

  } else {
    // print usage;
  }

  return 0;
}

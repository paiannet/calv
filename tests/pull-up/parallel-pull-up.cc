#include <cstring>
#include <chrono>
#include <iostream>
#include <omp.h>

#define NB_THREAD 9
#define LEFT 0
#define RIGHT 1

static inline int
_greatest_bit_pos(unsigned int n) {
  return 8*sizeof(n) - __builtin_clz(n);
}

inline int
go_1based(int base, int dir, int times) {
  if (times < 0) {
    return std::max(0, base >> (- times));
  } else {
    return ((base + dir) << times) - dir;
  }
}

inline int
go_0based(int base, int dir, int times) {
  return go_1based(base + 1, dir, times) - 1;
}

  int
main (int ac, char *av[])
{

  int sz = 0;
  int times;

  if (ac == 3
      && (sz = std::strtol(av[1], NULL, 10))
      && (times = std::strtol(av[2], NULL, 10))) {
    int * array = new int[(1 << (sz + 1)) - 2];

#pragma omp parallel for num_threads(NB_THREAD)
    for (int i = 0; i < (1 << (sz + 1)) - 2 ; ++i) {
      array[i] = i;
    }

    /* for (int i = (1 << sz)*0 ; i < (1 << (sz + 1)) - 2 ; ++i) { */
    /*   printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : ""); */
    /* } */
    /* putchar('\n'); */

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    for (int t = 0 ; t < times ; ++t) {
      int idx = 1;
      int len = 1 << (sz + 1);
      int start_lvl = _greatest_bit_pos(idx + 1);
      int end_lvl   = _greatest_bit_pos(len - 1);
      int n = end_lvl - start_lvl;
      int dir = (idx + 1) & 1;

      for (int j = 0 ; j <= n ; j++) {
#pragma omp parallel for num_threads(NB_THREAD)
        for (int i = 0 ; i < n - j ; ++i) {
          int depth = 1 << i;
          int size  = depth * (sizeof *array);
          int dst = go_0based(go_0based(go_0based(idx, dir, j - 1), 1 - dir, 1), LEFT, i);
          int src = go_0based(go_0based(go_0based(idx, dir, j), 1 - dir, 1), LEFT, i);

          /* printf("sending %d to %d (%d)\n", src, dest, depth); */
          memcpy(array + dst, array + src, size);
        }

        /* printf("arr[%d] = arr[%d]\n", cur_root / 2 - 1, cur_root - 1); */
        array[go_0based(idx, dir, j - 1)] = array[go_0based(idx, dir, j)];
      }

      // Clean up, memset 0 deepest level below (idx + 1) / 2 - 1
      /* printf("clear %d with %d zeroes\n", */
      /*     (1 << (end_lvl - start_lvl + 1)) * ((idx + 1) / 2) - 1, */
      /*     (1 << (end_lvl - start_lvl))); */
      memset(
          array + (1 << (end_lvl - start_lvl + 1)) * ((idx + 1) / 2) - 1,
          0,
          ((1 << (end_lvl - start_lvl + 1)) - 1) * (sizeof *array));

    }

    /* for (int i = (1 << sz)*0 ; i < (1 << (sz + 1)) - 2 ; ++i) { */
    /*   printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : ""); */
    /* } */
    /* putchar('\n'); */

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::microseconds>((end - begin) / times).count() << std::endl;
    delete [] array;
  }

  return 0;
}

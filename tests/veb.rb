#!/usr/bin/env ruby

# Status: Experimental.
# This file is here to explore the veb layout

@i = 1
nb = 15

@s = 15

@arr = Array.new 15

def compute_veb deb, fin
  @s = @s - 1
  # exit if @s == 0
  p "compute_veb #{deb}, #{fin}"
  if deb == fin then
    @arr[deb] = @i
    @i += 1
  else
    h_tot = (Math.log2(fin - deb + 1) + 1).to_i
    new_deb = deb + (2 ** (h_tot / 2)).to_i - 1
    new_size = ((fin - (2 ** (h_tot / 2) - 1) + 1 - deb) / (2 ** (h_tot / 2)))

    p "deb: #{deb}, fin: #{new_deb - 1}"
    ((2 ** (h_tot / 2))).times do |i|
      p "deb: #{new_deb + i * new_size}, fin: #{new_deb + (i + 1) * new_size - 1}"
    end
    puts "\n"
    compute_veb(deb, new_deb - 1)
    ((2 ** (h_tot / 2))).times do |i|
      compute_veb(new_deb + i * new_size, new_deb + (i + 1) * new_size - 1)
    end
  end
end

compute_veb(0, 63)
p @arr

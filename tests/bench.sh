#!/usr/bin/env bash

# : ${PERF:=perf}

# if [[ $(cat /proc/sys/kernel/perf_event_paranoid ) != 1 ]]; then
#     sudo sh -c 'echo 1 >/proc/sys/kernel/perf_event_paranoid'
# fi

compile () {
    g++ -O3 parallel-pull-down.cc -o pull-down-sequential
    g++ -O3 parallel-pull-down.cc -fopenmp -o pull-down-openmp
    g++ -O3 parallel-pull-down-mutex.cc -pthread -o pull-down-pthread

    g++ -O3 parallel-pull-up.cc -o pull-up-sequential
    g++ -O3 parallel-pull-up.cc -fopenmp -o pull-up-openmp
    g++ -O3 parallel-pull-up-mutex.cc -pthread -o pull-up-pthread

    g++ -O3 parallel-shift.cc -o shift-sequential
    g++ -O3 parallel-shift.cc -fopenmp -o shift-openmp
    g++ -O3 parallel-shift-mutex.cc -pthread -o shift-pthread

    g++ -O3 test-map.cc -I../include -o map-sequential
    g++ -O3 test-map.cc -I../include -fopenmp -o map-openmp
}

# 1 - backend: perf, time
# 2 - the file to test: parallel shift, parallel pull down or parallel pull up
# 3 - size, currently max is 29, to prevent overflowing...
# 4 - times, the number of time the program should run the operation
# 5 - openmp, pthread or sequential
run-test () {
    size=$3
    times=$4

    execfile="./$2-$5"

    if [ "$1" = "time" ] ; then
      if [ "$2" = "map" ] ; then
        awktime="/difference/ {time=\$5} END { print time }"

        printf "%s-%s %s %s %s " $2 $6 $5 $size $times
        $execfile $6 $size $times 2>&1 | awk "$awktime"
      else
        awktime="/%/ {cpu=\$1; total_time=\$2/$times} END { print cpu","total_time","time }"

        printf "%s %s %s %s " $2 $5 $size $times
        /usr/bin/time -f "%P %e" $execfile $size $times 2>&1 | awk "$awktime"
      fi

    else
    events=( "task-clock" "cache-misses" "cache-references" "branches" "branch-misses" )
    awkperf="
/task-clock/ { cpu_usage=\$5 }
/cache-misses/ { cache_miss=\$4 }
/branch-misses/ { branch_miss=\$4 }
/elapsed/ { clock=\$1/$times }
/difference/ { time=\$5 }
END { print time","clock","cpu_usage","cache_miss","branch_miss }"
    cmd="$PERF stat -e $(echo ${events[*]} | tr " " ",") $execfile $size $times"
    printf "%s %s %s %s " $1 $4 $size $times
    $cmd 2>&1 | awk "$awkperf"
    fi
}

# printf "operation implementation size n time total-time cpu_usage cache-miss branch-miss\n"
printf "operation implementation size n total-time time\n"

compile

for operation in "shift" "pull-up" "pull-down" ; do
    for size in $(seq 5 2 29) ; do
            for implem in "pthread" "openmp" "sequential" ; do
                run-test time $operation $size 100 $implem
            done;
    done;
done;

for operation in "map" ; do
    for size in $(seq 5 2 29) ; do
            for implem in "openmp" "sequential" ; do
                run-test time $operation $size 100 $implem avl-bf
                run-test time $operation $size 100 $implem avl-tree
                run-test time $operation $size 100 $implem set
            done;
    done;
done;

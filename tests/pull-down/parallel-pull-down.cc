#include <cstring>
#include <chrono>
#include <iostream>
#include <omp.h>

#define NB_THREAD 9

static inline int
_greatest_bit_pos(unsigned int n) {
  return 8*sizeof(n) - __builtin_clz(n);
}

int
main (int ac, char *av[])
{

  int sz = 0;
  int times;

  if (ac == 3
      && (sz = std::strtol(av[1], NULL, 10))
      && (times = std::strtol(av[2], NULL, 10))) {
    int * array = new int[(1 << (sz + 1)) - 2];
#pragma omp parallel for num_threads(NB_THREAD)
    for (int i = 0; i < (1 << sz) - 1 ; ++i) {
      array[i] = i + 1;
    }

    // for (int i = (1 << sz)*0 ; i < (1 << (sz + 1)) - 1 ; ++i) {
    //   printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : "");
    // }
    // putchar('\n');

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    for (int t = 0 ; t < times ; ++t) {
      int idx = 1;
      int len = 1 << sz;
      int dir = 1;
      int start_lvl = _greatest_bit_pos(idx + 1);
      int end_lvl   = _greatest_bit_pos(len - 1);

      for (int j = end_lvl ; j >= start_lvl ; --j) {
        int cur_root = (idx + 1) * (1 << (j - start_lvl))
          + dir * ((1 << (j - start_lvl)) - 1);

        array[2 * cur_root + dir - 1] = array[cur_root - 1];

#pragma omp parallel num_threads(NB_THREAD)
        for (int i = 0 ; i < end_lvl - (j + 1) + 1 ; ++i) {
          int depth = 1 << i;
          int size  = depth * (sizeof *array);
          int src   = depth * (2 * cur_root + (1 - dir)) - 1;
          int dest  = depth * (2 * (2 * cur_root + dir) + (1 - dir)) - 1;

          memcpy(array + dest, array + src, size);
        }
      }

      /* Clean up */
      array[(idx + 1) - 1] = 0;
      for (int i = 0 ; i < end_lvl - start_lvl ; ++i) {
        int depth = 1 << i;
        int size  = depth * (sizeof *array);
        int dest  = depth * (2 * (idx + 1) + (1 - dir)) - 1;

        memset(array + dest, 0, size);
      }

      // for (int i = (1 << sz)*0 ; i < (1 << (sz + 1)) - 1 ; ++i) {
      //   printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : "");
      // }
      // putchar('\n');
    }
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Time difference (parallel-down) = " << std::chrono::duration_cast<std::chrono::microseconds>((end - begin) / times).count() << "[µs]" << std::endl;

    delete [] array;
  }

  return 0;
}

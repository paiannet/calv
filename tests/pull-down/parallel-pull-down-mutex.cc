#include <pthread.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <queue>
#include <cstring>

int * array;

struct job {
    int src, dst, sz;
    job()
        : src(0), dst(0), sz(0)
    {};
    job(int src_, int dst_, int sz_)
        : src(src_), dst(dst_), sz(sz_)
    {};
};

pthread_mutex_t mutex;
pthread_barrier_t barrier;
pthread_t threads[9];
std::queue<job> queue;
int end = 0;
int nb[] = {1,2,3,4,5,6,7,8,9};
int chunk = 4096*4096;

static inline int
_greatest_bit_pos(unsigned int n) {
    return 8*sizeof(n) - __builtin_clz(n);
}

void*
do_things (void* args) {
    int round = 1;
    while (true) {
        job my_job;
        // fprintf(stderr, "thread %d round %d\n", *(int *) args, round);
        // fprintf(stderr, "thread %d trying to acquire lock\n", *(int *) args);
        pthread_mutex_lock(&mutex);
        if (end) {
            pthread_mutex_unlock(&mutex);
            break;
        } else {
            // fprintf(stderr, "thread %d got the lock\n", *(int *) args);
            if (queue.empty()) {
                // fprintf(stderr, "thread %d: nothing to do\n", *(int *) args);
                pthread_mutex_unlock(&mutex);
                pthread_barrier_wait(&barrier);
                continue;
            } else {
                my_job = queue.front(); queue.pop();
                // fprintf(stderr,
                //   "thread %d acquire copy job: src: %d, dst: %d, sz: %d\n",
                //   *(int *) args, my_job.src, my_job.dst, my_job.sz);
            }
            // fprintf(stderr, "thread %d release the lock\n", *(int *) args);
            pthread_mutex_unlock(&mutex);
        }

        // Do the job
        if (my_job.dst == 0) {
            memset(array + my_job.src, 0, my_job.sz);
        } else {
            memcpy(array + my_job.dst, array + my_job.src, my_job.sz);
        }


        // fprintf(stderr, "thread %d call wait\n", *(int *) args);
        pthread_barrier_wait(&barrier);
        // fprintf(stderr, "thread %d over the barrier\n", *(int *) args);
        round++;
    }

    // fprintf(stderr, "thread %d terminates\n", *(int *) args);
    pthread_barrier_wait(&barrier);
    return 0;
}

int
main (int ac, char * av[])
{
    int sz;
    int times;

    if (ac == 3
        && (sz = std::strtol(av[1], NULL, 10))
        && (times = std::strtol(av[2], NULL, 10))) {

        // 1. initialize array
        // no need to fill it, what we want to measure is how fast we can move
        // values around
        array = new int[1 << (sz + 1)];
        // for (int i = 0; i < (1 << sz) - 1 ; ++i) {
        //     array[i] = i + 1;
        // }
        // puts("before pull down");
        // for (int i = (1 << sz)*0 ; i < (1 << (sz + 1)) - 1 ; ++i) {
        //   printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : "");
        // }
        // putchar('\n');

        // 2. Create the thread pool
        pthread_mutex_init(&mutex, NULL);
        pthread_barrier_init(&barrier, NULL, 10);

        for (int i = 0 ; i < 9 ; i++) {
            pthread_create(threads + i, NULL, do_things, nb + i);
            pthread_detach(threads[i]);
        }

        for (int t = 0 ; t < times ; ++t) {
            fprintf(stderr, "time %d\n", t);
            // 3. initialize the shift parameters
            int idx = 1;
            int len_ = 1 << (sz + 1);
            int dir = 0;
            int start_lvl = _greatest_bit_pos(idx + 1);
            int end_lvl   = _greatest_bit_pos(len_ - 1);

            // 4. move a sub-tree
            for (int k = end_lvl ; k >= start_lvl ; --k) {
                int cur_root = (idx + 1) * (1 << (k - start_lvl))
                    + dir * ((1 << (k - start_lvl)) - 1);

                array[2 * cur_root + dir - 1] = array[cur_root - 1];
                // 4.1 move a layer
                for (int i = 0 ; i < end_lvl - (k + 1) - 1 ; ++i) {
                    int depth = 1 << i;
                    int size  = depth * (sizeof *array);
                    int src   = depth * (2 * cur_root + (1 - dir)) - 1;
                    int dst  = depth * (2 * (2 * cur_root + dir) + (1 - dir)) - 1;

                    // 4.1.1 Create copy jobs of size `chunk`.
                    for (int j = 0 ; j < size / chunk ; ++j) {
                        // fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
                        //   src + j * chunk / (sizeof *array),
                        //   dst + j * chunk / (sizeof * array),
                        //   chunk);
                        job job(src + j * chunk / (sizeof * array),
                                dst + j * chunk / (sizeof * array),
                                chunk);
                        pthread_mutex_lock(&mutex);
                        queue.push(job);
                        pthread_mutex_unlock(&mutex);
                    }

                    // 4.1.2 Create a copy job for the leftover
                    int leftover = (size / chunk) * chunk;
                    job job(src + leftover / (sizeof * array),
                            dst + leftover / (sizeof * array),
                            size - leftover);
                    // fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
                    //   src + leftover / (sizeof * array),
                    //   dst + leftover / (sizeof * array),
                    //   size - leftover);
                    pthread_mutex_lock(&mutex);
                    queue.push(job);
                    pthread_mutex_unlock(&mutex);
                }

                // 4.2 scheduling point:
                // wait for all jobs to be done.
                while (true) {
                    pthread_mutex_lock(&mutex);
                    if (queue.empty()) {
                        pthread_mutex_unlock(&mutex);
                        // fprintf(stderr, "no more jobs\n");
                        break;
                    } else {
                        pthread_mutex_unlock(&mutex);
                        pthread_barrier_wait(&barrier);
                    }
                }
            }

            // 5. Clean up
            array[(idx + 1) - 1] = 0;
            for (int i = 0 ; i < end_lvl - start_lvl ; ++i) {
                int depth = 1 << i;
                int size  = depth * (sizeof *array);
                int dst  = depth * (2 * (idx + 1) + (1 - dir)) - 1;

                // 5.1 Create copy jobs of size `chunk`.
                for (int j = 0 ; j < size / chunk ; ++j) {
                    // fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
                    //   src + j * chunk / (sizeof *array),
                    //   dst + j * chunk / (sizeof * array),
                    //   chunk);
                    job job(dst + j * chunk / (sizeof * array), 0, chunk);
                    pthread_mutex_lock(&mutex);
                    queue.push(job);
                    pthread_mutex_unlock(&mutex);
                }

                // 5.2 Create a copy job for the leftover
                int leftover = (size / chunk) * chunk;
                job job(dst + leftover / (sizeof * array), 0, size - leftover);
                // fprintf(stderr, "Create copy job: src %ld, dst %ld, sz: %d\n",
                //   src + leftover / (sizeof * array),
                //   dst + leftover / (sizeof * array),
                //   size - leftover);
                pthread_mutex_lock(&mutex);
                queue.push(job);
                pthread_mutex_unlock(&mutex);
            }

            // 6. scheduling point:
            // wait for all clean jobs to be done.
            while (true) {
                pthread_mutex_lock(&mutex);
                if (queue.empty()) {
                    pthread_mutex_unlock(&mutex);
                    // fprintf(stderr, "no more jobs\n");
                    break;
                } else {
                    pthread_mutex_unlock(&mutex);
                    pthread_barrier_wait(&barrier);
                }
            }
        }

        // puts("after pull down");
        // for (int i = (1 << sz)*0 ; i < (1 << (sz + 1)) - 1 ; ++i) {
        //   printf("%d %s", array[i], !((i + 1) & (i + 2)) ? "\n" : "");
        // }
        // putchar('\n');

        // 7. delete array
        delete [] array;

        // 8. conclude the program
        pthread_mutex_lock(&mutex);
        end = 1;
        pthread_mutex_unlock(&mutex);
        pthread_barrier_wait(&barrier);

    } else {
        // print usage;
    }

    return 0;
}

#! /usr/env/python3

import matplotlib.pyplot as plt
import csv

curves = []

# plt.subplots(figsize=(7,4))

#with open('map-4xi5-5300U.csv', newline='') as csvfile:
with open('memcpy-test.csv', newline='') as csvfile:
    myreader = csv.reader(csvfile, delimiter=',')
    next(myreader) # skip header
    xaxis = set()

    for row in myreader:
        # 0:total_moved_size, 1:block_size, 2:nb_thread, 3:duration
        xaxis.add(int(row[1]))
        try:
            curves[int(row[2]) - 1].append(int(row[3]))
        except:
            curves.append([])
            curves[int(row[2]) - 1].append(int(row[3]))

    for k in range(0,len(curves)):
        plt.plot(sorted(list(xaxis)), curves[k], label= str(k + 1) + ' threads')

plt.yscale('log')
plt.ylabel('Total Time (micro seconds)', fontsize = 14)
plt.xlabel('Block size', fontsize = 14)
plt.legend()
plt.margins(0)
plt.tight_layout()
plt.grid(linestyle=':')
# plt.xticks(range(13,28))

plt.savefig('memcpy-test-plot.pdf')
plt.show()

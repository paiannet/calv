#include <pthread.h>
#include <iostream>
#include <cstring>
#include <chrono>
#include <queue>
#include <unistd.h>
#include <cstdio>

const int buf_size = (1 << 20);

#define NB_THREADS 9
#define NB_TIMES 1000
#define BLOCK_STEP (1 << 10)


struct job {
  int src_, dst_, sz_, *arr_;

  job()
    : src_(0), dst_(0), sz_(0), arr_(nullptr)
  {};
  job(int src, int dst, int sz, int* arr)
    : src_(src), dst_(dst), sz_(sz), arr_(arr)
  {};
};

pthread_cond_t impulse;
pthread_mutex_t lock;

int nb_jobs_done = 0;
int time_to_die = 0;
int sleeping_threads = 0;

pthread_t threads[NB_THREADS];
int thread_has_done[NB_THREADS] = {0};
int nb[NB_THREADS] = {0};

std::queue<job> jobs;

void *
do_things (void * args)
{
  while (true) {
    // at first, we are sleeping and waiting for an impulse
    pthread_mutex_lock(&lock);
    sleeping_threads += 1;

    // no loop here, we don't care about spurious wake up because
    // we'll immediatly go back to sleep if there is nothing to do
    // anyway.
    pthread_cond_wait(&impulse, &lock);
    pthread_mutex_unlock(&lock);

    // impulse has come; it's time to work
    while (true) {
      // if we have nothing to do, go back to sleep.
      int no_more_jobs = 1;
      pthread_mutex_lock(&lock);
      no_more_jobs = jobs.empty();
      if (no_more_jobs) break;
      // if we have things to do, acquire the job.
      job my_job = jobs.front(); jobs.pop();
      nb_jobs_done += 1;
      thread_has_done[(*(int*) args)] += 1;
      pthread_mutex_unlock(&lock);

      // begin: do job
      memcpy(my_job.arr_ + my_job.dst_, my_job.arr_ + my_job.src_, my_job.sz_);
      // end: do job
    }
    // only reachable through the break, therefore we have the lock
    // back to sleep unless it's time to die.
    if (time_to_die) break;
    pthread_mutex_unlock(&lock);
  }
  // dyin'
  pthread_mutex_unlock(&lock);

  return 0;
}

int
main (int argc, char * argv[]) {
  int buf[buf_size];
  int nb_threads = std::atoi(argv[1]);

  using time_unit = std::chrono::microseconds;

  // opening.
  pthread_mutex_init(&lock, nullptr);
  pthread_cond_init(&impulse, nullptr);

  for (int i = 0 ; i < nb_threads ; ++i) {
    nb[i] = i;
  }

  for (int i = 0 ; i < nb_threads ; ++i) {
    pthread_create(threads + i, nullptr, do_things, nb + i);
  }

  // begin: real program
  // 1. Fill the buffer
  for (int i = 0 ; i < buf_size ; ++i) {
    buf[i] = i;
  }

  // Move the first half of the buffer into the next half
  // with small chunks of size `i`.
  for (int i = BLOCK_STEP ; i < buf_size / 2 ; i += BLOCK_STEP) {
    // std::cout << "i: " << i << std::endl;
    auto begin = std::chrono::steady_clock::now();
    for (int t = 0 ; t < NB_TIMES ; ++t) {
      for (int j = 0 ; (j + 1) * i <= buf_size / 2 ; ++j) {
	/* main copies */
	// std::cout << "buf[" << j*i << ":" << j*i + i << "] -> " 
	// 	  << "buf[" << buf_size/2 + j*i << ":" << buf_size/2 + j*i + i << "]"
	// 	  << std::endl;
	job job(j * i, buf_size/2 + j * i, i * sizeof(*buf), buf);
	  // std::cout << sleeping_threads << std::endl;
	pthread_mutex_lock(&lock);
	jobs.push(job);

	if (sleeping_threads > 0) {
	  pthread_cond_signal(&impulse);
	  sleeping_threads -= 1;
	}
	pthread_mutex_unlock(&lock);
      }
      /* left over */
      int leftover_sz = (buf_size / 2) - i * ((buf_size / 2) / i);
      // std::cout << "leftover (" << leftover_sz << ")" << std::endl;
      // std::cout << "buf[" << buf_size / 2 - leftover_sz << ":" << buf_size / 2 << "] -> " 
      // 		<< "buf[" << buf_size - leftover_sz << ":" << buf_size << "]"
      // 		<< std::endl;
      job job(buf_size / 2 - leftover_sz, buf_size - leftover_sz , sizeof(*buf) * leftover_sz, buf);
      pthread_mutex_lock(&lock);
      jobs.push(job);

      if (sleeping_threads > 0) {
	pthread_cond_signal(&impulse);
	sleeping_threads -= 1;
      }
      pthread_mutex_unlock(&lock);

      // scheduling point:
      // wait for all clean jobs to be done.
      while (true) {
	int no_more_jobs;
	pthread_mutex_lock(&lock);
	no_more_jobs = jobs.empty();
	pthread_mutex_unlock(&lock);
	if (no_more_jobs) break;
      }
    }
    auto end = std::chrono::steady_clock::now();

    // total_moved_size, block_size, nb_threads, duration
    std::cout
      << buf_size / 2 << ", " // total_moved_size
      << i << ", " // block_size
      << nb_threads << ", " // nb_threads
      << std::chrono::duration_cast<time_unit>((end - begin) / NB_TIMES).count() // duration
      << std::endl;
  }
  // end: real program

  // closings.
  pthread_mutex_lock(&lock);
  time_to_die = 1;
  pthread_cond_broadcast(&impulse);
  pthread_mutex_unlock(&lock);

  for (int i = 0 ; i < nb_threads ; ++i) {
    pthread_join(threads[i], NULL);
  }

  // std::cout << "work units done: " << nb_jobs_done << std::endl;
  // for (int i = 0 ; i < NB_THREADS ; ++i) {
  //   std::cout
  //     << i << ": " << thread_has_done[i] << "("
  //     << (((int) (10000 * (thread_has_done[i] / ((double) nb_jobs_done)))) / ((double) 100))
  //     << ")" << std::endl;
  // }

  pthread_mutex_destroy(&lock);
  pthread_cond_destroy(&impulse);

  return 0;
}

#ifndef __H__AVL_ARRAY_BREADTH_FIRST__
#define __H__AVL_ARRAY_BREADTH_FIRST__

#define NB_THREAD 9

#include <iostream>
#include <vector>

/* The type T has to be partially orderable and must implement = and < */

namespace bbt {

  template <typename T>
  class avl_bf {
    public:
      avl_bf(float compression_threshold = 0.1);
      avl_bf(const avl_bf<T>& rhs);
      avl_bf(avl_bf<T>&& rhs);

      avl_bf<T>& operator=(avl_bf<T> rhs);

      ~avl_bf();

      double density();
      avl_bf<T>& compress();
      avl_bf<T>& insert(std::vector<T>& vals);
      avl_bf<T>& insert(T val);

      avl_bf<T>& remove(std::vector<T>& vals);
      avl_bf<T>& remove(T val);

      bool find(T val);
      /* find(std::vector<T> vals); */

      /* maybe not be completely in place */
      /* template<class U> avl_bf<U>& map(std::function<T,U> f); */
      /* in place */
      avl_bf<T>& map(T (*f)(T));

      template<class U> friend void swap(avl_bf<U>& fst, avl_bf<U>& snd);
      template<class U> friend std::ostream& operator<<(std::ostream& stream, const avl_bf<U>& matrix);

    private:
      std::pair<T, size_t> * elems_;
      size_t size_, len_;
      float compressing_threshold_;

      inline static int _cleft(int i) {
        return 2 * (i + 1) - 1;
      }

      inline static int _cright(int i) {
        return 2 * (i + 1);
      }

      inline static int _father(int i) {
        return (i + 1) / 2 - 1;
      }

      inline int _balance(int idx);
      inline int _height(int idx);

      void _pull_up(int idx);
      void _pull_down(int idx, int dir);
      void _shift(int idx, int dir);

      void _bf_left_rotate(int idx);
      void _bf_right_rotate(int idx);
      void _left_rotate(int idx);
      void _right_rotate(int idx);

  };
};

#include "avl-bf.tpp"

#endif /* __H__AVL_ARRAY_BREADTH_FIRST__ */

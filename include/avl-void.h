#ifndef __H__AVL_TREE_VOID__
#define __H__AVL_TREE_VOID__

struct node {
  struct node * left, * right;
  void * val;
  size_t height;
};

struct avl {
  struct node * root;
  size_t size;
  // cmp(a, b) =  1 if a < b
  //           = -1 if a > b
  //           =  0 if a = b
  int (*cmp)(const void *, const void *);
};

struct avl * avl_insert (void * val, struct avl * avl);
struct avl * avl_delete (int val, struct node * node);
struct node * avl_find (void * val, struct avl * avl);
void avl_free (struct avl * avl);

#endif /* __H__AVL_TREE_VOID__ */

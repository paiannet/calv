#ifndef __H__AVL_TREE__
#define __H__AVL_TREE__

namespace bbt {

  template<typename T>
  struct avl_tree_node {
    avl_tree_node * left_, * right_;
    T val_;
    size_t height_;

    avl_tree_node(T val);
    avl_tree_node(const avl_tree_node<T>& rhs);
    avl_tree_node(avl_tree_node<T>&& rhs);

    avl_tree_node<T>& operator=(avl_tree_node<T> rhs);

    ~avl_tree_node();

    template<typename U> friend void swap(avl_tree_node<U>& fst, avl_tree_node<U>& snd);
  };

  template <typename T>
  class avl_tree {
    private:
    avl_tree_node<T>* root_;
    avl_tree_node<T>* insert(T val, avl_tree_node<T>* node);
    avl_tree_node<T>* remove(T val, avl_tree_node<T>* node,
        avl_tree_node<T>** parent1, avl_tree_node<T>** parent2);
    bool find(T val, avl_tree_node<T>* node);
    void map(T (*f)(T), avl_tree_node<T>* node);

    public:
    avl_tree();
    avl_tree(const avl_tree<T>& rhs);
    avl_tree(avl_tree<T>&& rhs);

    avl_tree<T>& operator=(avl_tree<T> rhs);

    ~avl_tree();

    avl_tree<T>& insert(std::vector<T>& vals);
    avl_tree<T>& insert(T val);

    /* avl_tree<T>& remove(std::vector<T>& vals); */
    avl_tree<T>& remove(T val);

    bool find(T val);
    /* find(std::vector<T> vals); */

    avl_tree<T>& map(T (*f)(T));

    template<typename U> friend void swap(avl_tree<U>& fst, avl_tree<U>& snd);
    template<typename U> friend std::ostream& operator<<(std::ostream& stream, const avl_tree<U>& avl);
};

};

#include "avl-tree.tpp"

#endif /* __H__AVL_TREE__ */


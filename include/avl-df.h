#ifndef __H__AVL_ARRAY_DEPTH_FIRST__
#define __H__AVL_ARRAY_DEPTH_FIRST__

struct avl_df {
  size_t nelems, len;
  int *elems, *heights, *fathers, *rights;
};

struct avl_df * avl_insert (int val, struct avl_df * avl);
struct avl_df * avl_delete (int val, struct avl_df * avl);
struct avl_df * avl_copy (struct avl_df * avl);
int avl_find (int val, struct avl_df * avl);
void avl_map (int (*f)(int), struct avl_df * avl);
void avl_print(struct avl_df * val);
void avl_free(struct avl_df * avl);

#endif /* __H__AVL_ARRAY_DEPTH_FIRST__ */


#include <stack>

namespace bbt {

template<typename T> class avl_bf;

#define LEFT       0
#define RIGHT      1

static inline int
_greatest_bit_pos(unsigned int n) {
  return 8*sizeof(n) - __builtin_clz(n);
}

template<typename T>
avl_bf<T>::avl_bf(float compressing_threshold)
  : elems_(nullptr)
  , size_(0)
  , len_(0)
  , compressing_threshold_(compressing_threshold)
{}

template<typename T>
avl_bf<T>::avl_bf(const avl_bf<T>& rhs)
  : avl_bf()
{
  size_ = rhs.size_;
  len_  = rhs.len_;

  elems_ = new std::pair<T, size_t>[len_];

  for (int i = 0 ; i < len_ ; ++i) {
    elems_[i] = rhs.elems_[i];
  }

}

template<typename T>
avl_bf<T>::avl_bf(avl_bf<T>&& rhs)
  : avl_bf()
{
  swap(*this, rhs);
}

template<typename T>
avl_bf<T>& avl_bf<T>::operator=(avl_bf<T> rhs)
{
  swap(*this, rhs);
  return *this;
}

template<typename T>
avl_bf<T>::~avl_bf()
{
  delete [] elems_;
}

template<typename T>
double avl_bf<T>::density() {
  return size_ / static_cast<double>( 1 << elems_[0].second );
}

static int
find_root(int n) {
  int m = n / 3;
  return m == 0 ? 0 : (2 << _greatest_bit_pos(m) - 1) - 1;
}

template<typename T>
avl_bf<T>& avl_bf<T>::compress()
{
  /* 1. Create a sorted sequence from the values in the avl.
   *    - Postfix depth-first traversal
   *    - Clear the values from the avl once they are stored
   */
  // std::cerr << "compress" << std::endl;
  std::vector<T> sorted_seq;
  std::stack<std::pair<int, bool>> s;
  /* Before adding the root check wether it exists */
  s.push(std::make_pair(0, false));

  while (!s.empty()) {
    std::pair<int, bool> i = s.top(); s.pop();
    T elem = elems_[i.first].first;
    int cleft  = _cleft(i.first);
    int cright = _cright(i.first);
    bool left_is_leaf  = cleft  >= len_ || elems_[cleft].first  == 0;
    bool right_is_leaf = cright >= len_ || elems_[cright].first == 0;
    bool is_leaf = left_is_leaf && right_is_leaf;
    if (is_leaf || i.second == true) {
      sorted_seq.push_back(elem);
      elems_[i.first].first = 0;
      elems_[i.first].second = 0;
    } else {
      if (!right_is_leaf)
        s.push(std::make_pair(cright, false));
      s.push(std::make_pair(i.first, true));
      if (!left_is_leaf)
        s.push(std::make_pair(cleft, false));
    }
  }

  auto new_slices = std::vector<int>{ -1 };
  auto n = sorted_seq.size();
  auto lvl = _greatest_bit_pos(size_);

  for (int i = 0, cnt = 0; cnt < n && i < len_;) {
    new_slices.push_back(n); // push the length of the array
    std::vector<int> slices = std::move(new_slices);
    new_slices.clear();
    for (int j = 0 ; j + 1 < slices.size() && cnt < n && i < len_ ; j++, i++) {
      int b = slices[j]   + 1;
      int e = slices[j+1] - 1;
      // if the slice is non-empty find the root of the slice
      if (e - b + 1 > 0) {
        int root_pos = b + find_root(e - b + 1);
        elems_[i] = std::make_pair(sorted_seq[root_pos], lvl);
        new_slices.push_back(b - 1);
        new_slices.push_back(root_pos);
        cnt++;
      } else {
        elems_[i] = std::make_pair(T(), 0);
      }
      lvl = lvl - !((i + 1) & (i + 2));
    }
  }

  size_ = n;

  return *this;
}


template<typename T>
avl_bf<T>& avl_bf<T>::insert(std::vector<T>& vals)
{
  for (auto& val : vals)
    insert(val);
  return *this;
}

template<typename T>
avl_bf<T>& avl_bf<T>::insert(T val)
{
  int i = 0;
  int balance = 0;

  if (len_ > 0) {
    /* 1. Standard BST insertion */

    /* The following loop traverses the tree to find where the next element has
     * to be inserted, `cond` is computed separately so that there is no need to
     * branch on in. */
    std::pair<T, size_t> * elems = elems_;
    while (i < len_ && elems->second) {
      int cond = val < elems->first;
      elems   += (2 * (i + 1) - cond) - i;
      i        = (2 * (i + 1) - cond);
    }

    if (len_ <= i) {
      int                   new_len   = 1 <<  _greatest_bit_pos(i);
      std::pair<T, size_t> *new_elems = new std::pair<T, size_t>[new_len]();

      for (int i = 0 ; i < len_ ; ++i) {
        new_elems[i] = elems_[i];
      }

      delete [] elems_;

      elems_ = new_elems;
      len_   = new_len;
    }

    elems_[i].first = val;
    elems_[i].second = 1;
    size_++;

    /* 2. Get back to the first unbalanced node */

    do {
      i = _father(i);
      elems_[i].second = _height(i);
    } while (i > 0 && abs(_balance(i)) < 2);

    /* 3. Rebalance */

    balance = _balance(i);

    if (balance < -1) {
      bool cond = val < elems_[_cright(i)].first;
      if (cond) {
        _right_rotate(_cright(i));
        _left_rotate(i);
      } else {
        _left_rotate(i);
      }
    }

    if (balance > 1) {
      bool cond = val < elems_[_cleft(i)].first;
      if (cond) {
        _right_rotate(i);
      } else {
        _left_rotate(_cleft(i));
        _right_rotate(i);
      }
    }

    /* 4. Recompute heights */

    while (i > 0) {
      i = _father(i);
      elems_[i].second = _height(i);
    }

  } else {
    elems_ = new std::pair<T, size_t>[1]();
    size_  = 1;
    len_   = 1;

    elems_[0].first  = val;
    elems_[0].second = 1;

  }

  if (density() - compressing_threshold_ < 0) {
    compress();
  }

  return *this;

}

template<typename T>
avl_bf<T>& avl_bf<T>::remove(std::vector<T>& vals) {
  for (auto& val : vals)
    remove(val);
  return *this;
}

template<typename T>
avl_bf<T>& avl_bf<T>::remove(T val)
{
  /* *
   *  1. Find the node `n` to delete
   *  2. Replace `n` with the bottom-most left-most node of the right child of
   *  `n`, let's call it `m`
   *  3. Remove this node.
   *  4. Rebalance the tree.
   */

  /* Find the node to delete */
  int i = 0;
  while (i < len_ && elems_[i].first != val) {
    int cond = val < elems_[i].first;
    i = 2 * (i + 1) - cond;
  }

  /* If the node has not be found, return now */
  if (i >= len_)
    return *this;

  /* std::cerr << "Preparing to delete node " << i << "\n"; */

  int i0 = i;
  /* Find the leaf that will be removed */

  /* 1. Check whether we are on a leaf */
  if (elems_[i].second == 1) {
    /* 2.1. It's a leaf! */
  } else {
    if (elems_[_cleft(i)].second) {
      /* 2.1. Shall we swap with the biggest leaf on the left */
      i = _cleft(i);
      while (i < len_ && elems_[i].second) {
        /* 3. Go right if we can, left otherwise*/
        int cond = 1 - !(_cright(i) < len_ && elems_[_cright(i)].second);
        i = (2 * (i + 1) - cond);
      }
    } else {
      /* 2.1'. ...or with the smallest leaf on the right */
      i = _cleft(i);
      while (i < len_ && elems_[i].second) {
        /* 3. Go left if we can, right otherwise*/
        int cond = !(_cright(i) < len_ && elems_[_cright(i)].second);
        i = (2 * (i + 1) - cond);
      }
    }
    /* 2.2. We've been a bit too far, roll back */
    i = _father(i);

    /* 2.3 Swap */
    /* std::cerr << "Preparing to swap nodes " << i << " and " << i0 << ".\n"; */

    T tmp = elems_[i0].first;
    elems_[i0].first = elems_[i].first;
    elems_[i].first  = tmp;
  }

  /* Remove */
  /* std::cerr << "Removing leaf " << i << ".\n"; */

  elems_[i].first  = T();
  elems_[i].second = 0;

  /* Rebalancing */
  while (i > 0) {
    /* 1. Get back to the first unbalanced node */
    do {
      i = _father(i);
      elems_[i].second = _height(i);
    } while (i > 0 && abs(_balance(i)) < 2);

    /* 2. Rebalance */

    int balance = _balance(i);

    if (balance < -1) {
      T val = elems_[_cright(_cright(i))].second
        ? elems_[_cright(_cright(i))].first
        : elems_[_cright(_cleft(i))].first;
      bool cond = val < elems_[_cright(i)].first;
      if (cond) {
        _right_rotate(_cright(i));
        _left_rotate(i);
        /* std::cerr << "RL at node " << i << "\n"; */
      } else {
        _left_rotate(i);
        /* std::cerr << "L at node " << i << "\n"; */
      }
    }

    if (balance > 1) {
      T val = elems_[_cleft(_cleft(i))].second
        ? elems_[_cleft(_cleft(i))].first
        : elems_[_cleft(_cright(i))].first;
      bool cond = val < elems_[_cleft(i)].first;
      if (cond) {
        _right_rotate(i);
        /* std::cerr << "R at node " << i << "\n"; */
      } else {
        _left_rotate(_cleft(i));
        _right_rotate(i);
        /* std::cerr << "LR at node " << i << "\n"; */
      }
    }
  }

  return *this;
}

template<typename T>
bool avl_bf<T>::find(T val)
{
  int nb = 0;
  int old_i = 0;

  if (size_ == 0) return -1;
  else {
    int i = 0;
    while (i < len_) {
      old_i = i;
      nb  += 1;
      if (val < elems_[i].first) {
        i = 2 * (i + 1) - 1;
      } else if (val > elems_[i].first) {
        i = 2 * (i + 1);
      } else {

        /* return i; */
        return true;
      }
    }
  }

  /* return -1; */
  return false;
}

/* f has to be monotone */
template<typename T>
avl_bf<T>& avl_bf<T>::map(T (*f)(T)) {
  #pragma omp parallel for num_threads(NB_THREAD)
  for (int i = 0 ; i < len_ ; ++i) {
    elems_[i].first = f(elems_[i].first);
  }

  return *this;
}

template<typename U>
std::ostream& operator<<(std::ostream& stream, const avl_bf<U>& avl)
{
  int i = 0;
  stream << "digraph {\n";
  if (avl.elems_[i].second > 1) {
    for (i = 0 ; i < avl.len_ ; ++i) {
      if (avl._cleft(i) < avl.len_ && avl.elems_[avl._cleft(i)].second)
        stream << "s" << i + 1 << "_" << avl.elems_[i].first << " -> "
               << "s" << avl._cleft(i) + 1 << "_" << avl.elems_[avl._cleft(i)].first << "\n";
      if (avl._cright(i) < avl.len_ && avl.elems_[avl._cright(i)].second)
        stream << "s" << i + 1 << "_" << avl.elems_[i].first << " -> "
               << "s" << avl._cright(i) + 1 << "_" << avl.elems_[avl._cright(i)].first << "\n";
    }
  } else {
    if (avl.elems_[i].second != 0)
      stream << "s1_" << avl.elems_[i].first << ".\n";
  }
  stream << "}\n";

  return stream;
}

template<typename U>
void swap(avl_bf<U>& first, avl_bf<U>& second)
{
  using std::swap;

  swap(first.elems_, second.elems_);
  swap(first.size_,  second.size_);
  swap(first.len_,   second.len_);
}

/* void */
/* avl_bf::avl_map (int (*f)(int), struct avl_bf * avl) { */
/*   int * elems_ = avl->elems_; */
/*   for (int i = 0 ; i < avl->len_ ; ++i) */
/*     if (elems_[i] != 0) */
/*       elems_[i] = f(elems_[i]); */
/* } */

/* dir = 0 is left, 1 is right */
/* `elems_` has to be an array of `len_` and `len_` has to be a number of the form 2^n */
template<typename T>
void avl_bf<T>::_pull_down(int idx, int dir)
{
  int start_lvl = _greatest_bit_pos(len_ - 1);
  int end_lvl   = _greatest_bit_pos(idx + 1);

  /* First level we don't know */
  int depth = 1 << (start_lvl - end_lvl);
  int size  = depth * (sizeof *elems_);
  int dest  = depth * (2 * (idx + 1) + dir) - 1;
  int src   = depth * (idx + 1) - 1;

  if (dest + depth < len_)
    memcpy(elems_ + dest, elems_ + src, size);
  else if (dest < len_)
    memcpy(elems_ + dest, elems_ + src, (len_ - dest) * (sizeof *elems_));

  if (src + depth < len_)
    memset(elems_ + src, 0, size);
  else if (src < len_)
    memset(elems_ + src, 0, (len_ - src) * (sizeof *elems_));

  for (int i = start_lvl - 1; end_lvl <= i ; --i) {
    int depth = 1 << (i - end_lvl);
    int size  = depth * (sizeof *elems_);
    int dest  = depth * (2 * (idx + 1) + dir) - 1;
    int src   = depth * (idx + 1) - 1;

    memcpy(elems_ + dest, elems_ + src, size);
    memset(elems_ + src, 0, size);
  }
}

template<typename T>
void avl_bf<T>::_pull_up(int idx)
{
  int start_lvl = _greatest_bit_pos(idx + 1) - 1;
  int end_lvl   = _greatest_bit_pos(len_)     + 1;
  int steps     = end_lvl - start_lvl;

  for (int i = 0 ; i < steps - 2 ; ++i) {
    int depth = 1 << i;
    int size  = depth * (sizeof *elems_);
    int dest  = depth * ((idx + 1) / 2) - 1;
    int src   = depth * (idx + 1) - 1;

    memcpy(elems_ + dest, elems_ + src, size);
    memset(elems_ + src, 0, size);
  }

  for (int i = steps - 2 ; i < steps ; ++i) {
    int depth = 1 << i;
    int size  = depth * (sizeof *elems_);
    int dest  = depth * ((idx + 1) / 2) - 1;
    int src   = depth * (idx + 1) - 1;

    if (src + depth < len_) {
      memmove(elems_ + dest, elems_ + src, size);
      memset(elems_ + src, 0, size);
    } else {
      if (dest + depth < len_) {
        memset(elems_ + dest, 0, size);
      } else if (dest < len_) {
        memset(elems_ + dest, 0, (len_ - dest) * (sizeof *elems_));
      }
      if (src < len_) {
        memmove(elems_ + dest, elems_ + src, (len_ - src) * (sizeof *elems_));
        memset(elems_ + src, 0, (len_ - src) * (sizeof *elems_));
      }
    }
  }
}

/* dir: left is 0, right is 1 */
template<typename T>
void avl_bf<T>::_shift(int idx, int dir)
{
  int start_lvl = _greatest_bit_pos(idx + 1);
  int end_lvl   = _greatest_bit_pos(len_ - 1);
  int steps     = end_lvl - start_lvl + 1;
  int dir_coef  = 2 * dir - 1; /* 0 -> -1 ; 1 -> 1 */

  for (int i = 0 ; i < steps ; ++i) {
    int depth = 1 << i;
    int size  = depth * (sizeof *elems_);
    int src   = depth * (idx + 1) - 1;
    int dest  = src + dir_coef * depth;

    /* src and dest are on the same level */
    /* if (__builtin_clz(dest + 1) == __builtin_clz(src + 1)) { */
    /*   if (dest + depth < len_) { */
    /*     // can write at undesired places */

        memcpy(elems_ + dest, elems_ + src, size);
    /*   } else if (dest < len_) { */
    /*     memmove(elems_ + dest, elems_ + src, (len_ - dest) * (sizeof *elems_)); */
    /*   } */
    /* } */

    /* set 0s */
    /* if (src + depth < len_) { */
      memset(elems_ + src, 0, size);
    /* } else if (src < len_) { */
    /*   memset(elems_ + src, 0, (len_ - src) * (sizeof *elems_)); */
    /* } */
  }
}

template<typename T>
void avl_bf<T>::_bf_left_rotate(int idx)
{
  std::pair<T, size_t> a = elems_[idx];
  std::pair<T, size_t> b = elems_[_cright(idx)];
  elems_[idx].first = T();
  elems_[_cright(idx)].first = T();

  _pull_down(_cleft(idx), LEFT);
  _shift(_cleft(_cright(idx)), LEFT);
  _pull_up(_cright(_cright(idx)));

  elems_[_cleft(idx)] = a;
  elems_[idx]         = b;
}

template<typename T>
void avl_bf<T>::_bf_right_rotate(int idx)
{
  std::pair<T, size_t> a = elems_[idx];
  std::pair<T, size_t> b = elems_[_cleft(idx)];
  elems_[idx].first = T();
  elems_[_cleft(idx)].first = T();

  _pull_down(_cright(idx), RIGHT);
  _shift(_cright(_cleft(idx)), RIGHT);
  _pull_up(_cleft(_cleft(idx)));

  elems_[_cright(idx)] = a;
  elems_[idx]          = b;
}

template<typename T>
inline int avl_bf<T>::_balance(int idx)
{
  int left  = _cleft(idx)  < len_ ? elems_[_cleft(idx)].second  : 0;
  int right = _cright(idx) < len_ ? elems_[_cright(idx)].second : 0;

  return left - right;
}

template<typename T>
inline int avl_bf<T>::_height(int idx)
{
  int left  = _cleft(idx)  < len_ ? elems_[_cleft(idx)].second  : 0;
  int right = _cright(idx) < len_ ? elems_[_cright(idx)].second : 0;

  return 1 + (left > right ? left : right);
}

template<typename T>
void avl_bf<T>::_left_rotate(int idx)
{
  _bf_left_rotate(idx);
  elems_[_cleft(idx)].second = _height(_cleft(idx));
  elems_[idx].second         = _height(idx);
}

template<typename T>
void avl_bf<T>::_right_rotate(int idx)
{
  _bf_right_rotate(idx);
  elems_[_cright(idx)].second = _height(_cright(idx));
  elems_[idx].second          = _height(idx);
}
}

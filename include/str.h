#ifndef __H__STR__
#define __H__STR__

#include <cstring>
#include <iostream>

struct str {
  char ** s_;

  str () : s_(nullptr) { }

  str (const char * s) : str() {
    if (s != nullptr) {
      s_ = new (char*);
      *s_ = new char [strlen(s) + 1];
      strcpy(*s_, s);
    }
  }

  str(const str& rhs) : str() {
    if (rhs.s_ && *rhs.s_) {
      s_ = new (char*);
      *s_ = new char [strlen(*rhs.s_) + 1];
      strcpy(*s_, *rhs.s_);
    }
  }

  str(str&& rhs) : str() {
    swap(*this, rhs);
  }

  ~str() {
    if (s_ && *s_) delete [] *s_;
    if (s_)        delete s_;
  }

  str& operator=(str rhs) {
    swap(*this, rhs);
    return *this;
  }

  bool operator< (const str& rhs) const {
    if (s_ && rhs.s_)
      return strcmp(*s_, *rhs.s_) < 0;
    return false;
  }

  bool operator<= (const str& rhs) const {
    if (s_ && rhs.s_)
      return strcmp(*s_, *rhs.s_) <= 0;
    return false;
  }

  bool operator>= (const str& rhs) const {
    if (s_ && rhs.s_)
      return strcmp(*s_, *rhs.s_) >= 0;
    return false;
  }

  bool operator> (const str& rhs) const {
    if (s_ && rhs.s_)
      return strcmp(*s_, *rhs.s_) > 0;
    return false;
  }

  bool operator== (const str& rhs) const {
    if (s_ && rhs.s_)
      return strcmp(*s_, *rhs.s_) == 0;
    return false;
  }

  bool operator!= (const str& rhs) const {
    if (s_ && rhs.s_)
      return strcmp(*s_, *rhs.s_) != 0;
    return false;
  }

  friend void swap(str& fst, str& snd) {
    using std::swap;

    swap(fst.s_, snd.s_);
  }

  friend std::ostream& operator<<(std::ostream& stream, const str str) {
    stream << *str.s_;
    return stream;
  }

};

#endif /* __H__STR__ */

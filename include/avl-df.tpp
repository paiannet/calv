#include <assert.h>
#include <time.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "avl-df.h"

static int
my_rand (int min, int max) {
  return (int) ((max - min) * (rand() / (float) RAND_MAX) + min);
}

static inline int
_greatest_bit_pos(unsigned int n) {
  return 8*sizeof(n) - __builtin_clz(n);
}

static void
_df_left_rotate (int idx, int * elems, int * rights, size_t len) {
  int a  = elems[idx];
  int b  = elems[rights[idx]];

  memmove(elems + idx + 2, elems + idx + 1, (rights[idx] - idx - 1) * (sizeof *elems));

  elems[idx + 1] = a;
  elems[idx]     = b;
}

static void
_df_right_rotate (int idx, int * elems, int * rights, size_t len) {
  /* int a  = elems[idx]; */
  /* int b  = elems[idx + 1]; */
  /* int c  = elems[idx + 2]; */

  /* memmove(elems + idx + 2, elems + idx + 3, ((rights[idx] + 1) - (idx + 2) - 1) * (sizeof *elems)); */

  /* elems[right[idx]] = a; */
  /* elems[idx + 1]    = c; */
  /* elems[idx]        = b; */
}

static void
_df_add_cst(int cst, int st, int ed, int * tab, int len) {
  /* assert(0 <= st && st < len); */
  /* assert(0 <= ed && ed < len); */

  /* for (int i = st ; i < ed ; ++i) */
  /*   tab[i] = tab[i] + (tab[i] > 0 ? cst : 0); */
}

static inline int
_balance (int idx, struct avl_df * avl) {
  /* return avl->heights[idx + 1] - avl->heights[avl->rights[idx]]; */
  return 0;
}

static inline int
_height(int idx, int * heights, size_t len) {
  /* int left  = idx + 1 < len ? heights[idx + 1] : 0; */
  /* int right = avl->rights[idx] < len ? heights[avl->rights[idx]] : 0; */

  /* return 1 + (left > right ? left : right); */
  return 0;
}

static void
_left_rotate (int idx, struct avl_df * avl) {
  /* int st = idx + 1; */
  /* int ed = rights[idx] - idx - 1; */

  /* _df_left_rotate(idx, avl->elems, avl->rights avl->len); */
  /* _df_left_rotate(idx, avl->heights, avl->rights, avl->len); */

  /* _df_add_cst(1, idx + 1, rights[idx] - idx - 1, avl->fathers, avl->len); */

  /* avl->heights[idx + 1] = _height(idx + 1, avl->heights, avl->len); */
  /* avl->heights[idx] = _height(idx, avl->heights, avl->len); */
}


static void
_right_rotate (int idx, struct avl_df * avl) {
  /* int st = idx + 2; */
  /* int ed = (rights[idx] + 1) - (idx + 2) - 1; */
  /* _df_right_rotate(idx, avl->elems, avl->len); */
  /* _df_right_rotate(idx, avl->heights, avl->len); */

  /* _df_right_rotate(idx, avl->fathers, avl->len); */
  /* _df_add_cst(-1, st, ed, avl->fathers, avl->len); */
  /* avl->fathers[ed]               = idx; */
  /* avl->fathers[ed + 1]           = ed; */
  /* avl->fathers[avl->rights[idx]] = ed; */

  /* _df_right_rotate(idx, avl->rights, avl->len); */
  /* avl->rights[ed]      = avl->rights[idx]; */
  /* _df_add_cst(-1, st, ed, avl->rights, avl->len); */
  /* avl->rights[idx]     = avl->rights[idx + 1] - 1; */
  /* avl->rights[idx + 1] = avl->rights[idx + 2] + 1; */

  /* avl->heights[avl->rights[idx]] = _height(avl->rights[idx], avl->heights, avl->len); */
  /* avl->heights[idx] = _height(idx, avl->heights, avl->len); */
}

/* avl should not be NULL, return value is the index of insertion */
int
df_bst_insert (int val, struct avl_df * avl) {
  /* if (avl) { */
  /*   int i = 0, j = 0; */
  /*   while (i != -1 && i = avl->fathers[i + 1]  && i < avl->len) */
  /*     j = i, i = val < avl->elems[i] ? i + 1 : avl->rights[i]; */

  /*     /1* 1. Enlarge the array if there is not enough room *1/ */
  /*     if (avl->nelems == avl->len) { */
  /*       /1* ... *1/ */
  /*     } */

  /*     memmove(avl->elems + j + 2, avl->elems + j + 1, (avl->nelems - j + 2 + 1) * (sizeof *avl->elems)); */
  /*     memmove(avl->heights + j + 2, avl->heights + j + 1, (avl->nelems - j + 2 + 1) * (sizeof *avl->heights)); */


  /*     memmove(avl->rights + j + 2, avl->rights + j + 1, (avl->nelems - j + 2 + 1) * (sizeof *avl->rights)); */
  /*     _df_add_cst(1, j + 2, avl->nelems - j + 2 + 1, avl->rights); */
  /* } else { */
  /*   return -1; */
  /* } */
  return 0;
}

struct avl_df *
avl_insert (int val, struct avl_df * avl) {
  /* int i = 0; */
  /* int balance = 0; */

  /* if (avl) { */
  /*   /1* 1. Standard BST insertion *1/ */

  /*   while (i < avl->len && avl->heights[i]) */
  /*     i = val < avl->elems[i] ? CLEFT(i) : CRIGHT(i); */

  /*   if (avl->len <= i) { */
  /*     int   new_len     = 1 << _greatest_bit_pos(i); */
  /*     int * new_elems   = calloc(new_len, sizeof *(avl->elems)); */
  /*     int * new_heights = calloc(new_len, sizeof *(avl->heights)); */

  /*     memcpy(new_elems, avl->elems, avl->len * sizeof *(avl->elems)); */
  /*     memcpy(new_heights, avl->heights, avl->len * sizeof *(avl->elems)); */

  /*     free(avl->elems),   avl->elems   = new_elems; */
  /*     free(avl->heights), avl->heights = new_heights; */
  /*     avl->len = new_len; */
  /*   } */

  /*   avl->elems[i] = val; */
  /*   avl->heights[i] = 1; */
  /*   avl->nelems++; */

  /*   /1* 2. Get back to the first unbalanced node *1/ */

  /*   do { */
  /*     i = FATHER(i); */
  /*     avl->heights[i] = _height(i, avl->heights, avl->len); */
  /*   } while (i > 0 && abs(_balance(i, avl)) < 2); */

  /*   /1* 3. Rebalance *1/ */

  /*   balance = _balance(i, avl); */

  /*   if (balance < -1) { */
  /*     int cond = val < avl->elems[CRIGHT(i)]; */
  /*     if (cond > 0) { */
  /*       _right_rotate(CRIGHT(i), avl); */
  /*       _left_rotate(i, avl); */
  /*     } else { */
  /*       _left_rotate(i, avl); */
  /*     } */
  /*   } */

  /*   if (balance > 1) { */
  /*     int cond = val < avl->elems[CLEFT(i)]; */
  /*     if (cond > 0) { */
  /*       _right_rotate(i, avl); */
  /*     } else { */
  /*       _left_rotate(CLEFT(i), avl); */
  /*       _right_rotate(i, avl); */
  /*     } */
  /*   } */

  /*   /1* 4. Recompute heights *1/ */

  /*   while (i > 0) { */
  /*     i = FATHER(i); */
  /*     avl->heights[i] = _height(i, avl->heights, avl->len); */
  /*   } */

  /*   return avl; */
  /* } else { */
  /*   struct avl_df * _avl = calloc(1, sizeof *_avl); */

  /*   _avl->elems = calloc(1000000, sizeof *(_avl->elems)); */
  /*   _avl->heights = calloc(1000000, sizeof *(_avl->heights)); */
  /*   _avl->len    = 1000000; */
  /*   _avl->nelems = 1; */
  /*   _avl->elems[0] = val; */
  /*   _avl->heights[0] = 1; */

  /*   return _avl; */
  /* } */
  return NULL;
}

struct avl_df *
avl_delete (int val, struct avl_df * avl) {
  return avl;
}

void
avl_free (struct avl_df * avl) {
  /* free(avl->elems), avl->elems = NULL; */
  /* free(avl->heights), avl->heights = NULL; */
  /* free(avl), avl = NULL; */
} 

int
avl_find (int val, struct avl_df * avl) {
  if (!avl) return -1;
  else {
    int i = 0;
    while (i < avl->len) {
      if (val < avl->elems[i]) {
        i = 2 * (i + 1) - 1;
      } else if (val > avl->elems[i]) {
        i = 2 * (i + 1);
      } else {
        return i;
      }
    }
  }
  return -1;
}

struct avl_df *
avl_copy (struct avl_df * avl) {
  return NULL;
}

void
avl_print(struct avl_df * val) {
}

struct avl_df *
avl_random_fill (struct avl_df * avl, int min, int max, int size) {
   int n;
   struct avl_df * _avl = avl;
   while (size-- > 0) {
     n = my_rand(min, max);
     _avl = avl_insert(n, _avl);
   }
   return _avl;
 }

int
avl_random_search (struct avl_df * avl, int min, int max, int size) {
  int n;
  int count = 0;
  while (size-- > 0) {
    n = my_rand(min, max);
    count += avl_find(n, avl) != -1;
  }
  return count;
}

void
print_tab (int * elems, size_t len) {
  for (int i = 0 ; i < len ; ++i)
    printf("%d%s", elems[i], i == len - 1 ? "" : ":");
  puts("");
}

static int
count (int * tab, size_t len) {
  int cnt = 0;
  for (int i = 0 ; i < len ; ++i) {
    cnt += !!tab[i];
  }
  return cnt;
}

static void
test_pull_up (char * elems[], size_t len, size_t at) {
  /* int * tab = calloc(len, sizeof *tab); */

  /* for (int i = 0 ; i < len ; ++i) */
  /*   tab[i] = atoi(elems[i]); */

  /* _pull_up(at, tab, len); */
  /* print_tab(tab, len); */

  /* free(tab); */
}

static void
test_push_down (char * elems[], size_t len, size_t at) {
  /* int * tab = calloc(len, sizeof *tab); */
  /* int dir = !!strcmp(elems[0], "left"); */

  /* for (int i = 0 ; i < len ; ++i) */
  /*   tab[i] = atoi(elems[i + 1]); */

  /* _push_down(at, tab, len, dir); */
  /* print_tab(tab, len); */

  /* free(tab); */
}

static void
test_shift (char * elems[], size_t len, size_t at) {
  /* int * tab = calloc(len, sizeof *tab); */
  /* int dir = !!strcmp(elems[0], "left"); */

  /* for (int i = 0 ; i < len ; ++i) */
  /*   tab[i] = atoi(elems[i + 1]); */

  /* _shift(at, tab, len, dir); */
  /* print_tab(tab, len); */

  /* free(tab); */
}

static void
test_rotate(char * elems[], size_t len, size_t at) {
  /* int * tab = calloc(len, sizeof *tab); */
  /* int dir = !!strcmp(elems[0], "left"); */

  /* for (int i = 0 ; i < len ; ++i) */
  /*   tab[i] = atoi(elems[i + 1]); */

  /* if (dir == LEFT) */
  /*   _df_left_rotate(at, tab, len); */
  /* else */
  /*   _df_right_rotate(at, tab, len); */

  /* print_tab(tab, len); */

  /* free(tab); */
}

static void
test_insert(char * elems[], size_t len) {
  /* struct avl_df * avl = NULL; */

  /* for (int i = 0 ; i < len ; ++i) */
  /*   avl = avl_insert(atoi(elems[i]), avl); */

  /* print_tab(avl->elems, avl->len); */
  /* print_tab(avl->heights, avl->len); */

  /* avl_free(avl); */
}

static void
test_random_fill(char * av[], size_t len) {
  /* struct avl_df * avl = NULL; */
  /* int min    = atoi(av[0]); */
  /* int max    = atoi(av[1]); */
  /* int nelems = atoi(av[2]); */
  /* int print  = len == 4 && !strcmp(av[3], "print"); */

  /* avl = avl_random_fill(avl, min, max, nelems); */

  /* if (print) { */
  /*   print_tab(avl->elems, avl->len); */
  /*   print_tab(avl->heights, avl->len); */
  /*   printf("%d-%d\n", count(avl->elems, avl->len), count(avl->heights, avl->len)); */
  /* } */
}

static void
test_random_search(char * av[], size_t len) {
  /* struct avl_df * avl = NULL; */
  /* int min    = atoi(av[0]); */
  /* int max    = atoi(av[1]); */
  /* int nelems = atoi(av[2]); */
  /* int ntries = atoi(av[3]); */
  /* int print  = len == 5 && !strcmp(av[4], "print"); */
  /* int count  = 0; */

  /* avl = avl_random_fill(avl, min, max, nelems); */
  /* count = avl_random_search(avl, min, max, ntries); */

  /* if (print) { */
  /*   printf("%d hits accross %d searches.\n", count, ntries); */
  /* } */
}

#ifdef STANDALONE
int
main (int ac, char * av[]) {
  srand(time(NULL));

/*   if (ac > 1) { */
/*     if (!strcmp(av[1], "pull-up")) { */
/*       test_pull_up(av + 3, ac - 3, atoi(av[2])); */
/*     } else if (!strcmp(av[1], "push-down")) { */
/*       test_push_down(av + 3, ac - 4, atoi(av[2])); */
/*     } else if (!strcmp(av[1], "shift")) { */
/*       test_shift(av + 3, ac - 4, atoi(av[2])); */
/*     } else if (!strcmp(av[1], "rotate")) { */
/*       test_rotate(av + 3, ac - 4, atoi(av[2])); */
/*     } else if (!strcmp(av[1], "insert")) { */
/*       test_insert(av + 2, ac - 2); */
/*     } else if (!strcmp(av[1], "random-fill")) { */
/*       test_random_fill(av + 2, ac - 2); */
/*     } else if (!strcmp(av[1], "random-search")) { */
/*       test_random_search(av + 2, ac - 2); */
/*     } */
/*   } */

  return 0;
}
#endif

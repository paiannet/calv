/* #include <algorithm> */
/* #include <random> */
/* #include <memory> */
/* #include <stack> */
/* #include <queue> */
#include <vector>
#include <iostream>
/* #include <iomanip> */

namespace bbt {

template<typename T>
avl_tree_node<T>::avl_tree_node(T val)
  : left_(nullptr), right_(nullptr), val_(val), height_(1)
{
}

template<typename T>
avl_tree_node<T>::avl_tree_node(const avl_tree_node<T>& rhs)
{
  left_   = rhs.left_  ? new avl_tree_node(*rhs.left_)  : nullptr;
  right_  = rhs.right_ ? new avl_tree_node(*rhs.right_) : nullptr;
  val_    = rhs.val_;
  height_ = rhs.height_;
}

template<typename U>
void swap(avl_tree_node<U>& fst, avl_tree_node<U>& snd)
{
  using std::swap;

  swap(fst.left_,   snd.left_);
  swap(fst.right_,  snd.right_);
  swap(fst.val_,    snd.val_);
  swap(fst.height_, snd.height_);
}

template<typename T>
avl_tree_node<T>::avl_tree_node(avl_tree_node<T>&& rhs)
  : avl_tree_node()
{
  swap(*this, rhs);
}

template<typename T>
avl_tree_node<T>& avl_tree_node<T>::operator=(const avl_tree_node<T> rhs)
{
  swap(*this, rhs);
  return *this;
}

template<typename T>
avl_tree_node<T>::~avl_tree_node()
{
  delete left_;
  delete right_;
}

template<typename T>
avl_tree<T>::avl_tree()
  : root_(nullptr)
{}

template<typename T>
avl_tree<T>::avl_tree(const avl_tree<T>& rhs)
  : root_(new avl_tree_node<T>(*rhs.root_))
{
}

template<typename U>
void swap(avl_tree<U>& fst, avl_tree<U>& snd)
{
  using std::swap;

  swap(fst.root_,   snd.root_);
}

template<typename T>
avl_tree<T>::avl_tree(avl_tree<T>&& rhs)
  : avl_tree()
{
  swap(*this, rhs);
}

template<typename T>
avl_tree<T>& avl_tree<T>::operator=(avl_tree<T> rhs)
{
  swap(*this, rhs);
  return *this;
}

template<typename T>
avl_tree<T>::~avl_tree()
{
  delete root_;
}

template<typename T>
void _avl_print (avl_tree_node<T> * node, int * idx)
{
  int _idx = *idx;
  if (node) {
    if (node->left_) {
      (*idx)++;
      std::cout << "s" << _idx << "_" << node->val_ << " -> " << "s" << *idx << "_" << node->left_->val_ << ";\n";
      _avl_print (node->left_, idx);
    }
    if (node->right_) {
      (*idx)++;
      std::cout << "s" << _idx << "_" << node->val_ << " -> " << "s" << *idx << "_" << node->right_->val_ << ";\n";
      _avl_print (node->right_, idx);
    }
  }
}

template<typename U>
std::ostream& operator<<(std::ostream& stream, const avl_tree<U>& avl)
{
  int idx = 1;
  stream << "digraph {\n";
  if (avl.root_ && avl.root_->height_ == 1) {
    stream << "s" << idx << "_" << avl.root_->val_ << ";\n";
  } else {
    _avl_print(avl.root_, &idx);
  }
  std::cout << "}\n";
  return stream;
}

template<class T>
avl_tree_node<T>* _avl_right_rotate(avl_tree_node<T>* x)
{
  avl_tree_node<T>* y = x->left_;

  x->left_   = y->right_;
  int xlh = x->left_  ? x->left_->height_  : 0;
  int xrh = x->right_ ? x->right_->height_ : 0;
  x->height_ = 1 + std::max(xlh, xrh);

  y->right_  = x;
  int ylh = y->left_  ? y->left_->height_  : 0;
  int yrh = y->right_ ? y->right_->height_ : 0;
  y->height_ = 1 + std::max(ylh, yrh);

  return y;
}

template<class T>
avl_tree_node<T>* _avl_left_rotate(avl_tree_node<T>* x)
{
  avl_tree_node<T>* y = x->right_;

  x->right_ = y->left_;
  int xlh = x->left_  ? x->left_->height_  : 0;
  int xrh = x->right_ ? x->right_->height_ : 0;
  x->height_ = 1 + std::max(xlh, xrh);

  y->left_ = x;
  int ylh = y->left_  ? y->left_->height_  : 0;
  int yrh = y->right_ ? y->right_->height_ : 0;
  y->height_ = 1 + std::max(ylh, yrh);

  return y;
}

template<typename T>
avl_tree_node<T>* avl_tree<T>::insert(T val, avl_tree_node<T>* node)
{
  int balance = 0;

  if (!node)
    return new avl_tree_node<T>(val);

  if (val < node->val_) {
    node->left_ = insert(val, node->left_);
  } else {
    node->right_ = insert(val, node->right_);
  }

  int nlh = node->left_  ? node->left_->height_  : 0;
  int nrh = node->right_ ? node->right_->height_ : 0;
  node->height_ = 1 + std::max(nlh, nrh);
  balance = nlh - nrh;

  if (balance < -1) {
    int cond = val < node->right_->val_;
    if (cond > 0) {
      node->right_ = _avl_right_rotate(node->right_);
      node = _avl_left_rotate(node);
    } else {
      node = _avl_left_rotate(node);
    }
  }

  if (balance > 1) {
    int cond = val < node->left_->val_;
    if (cond > 0) {
      node = _avl_right_rotate(node);
    } else {
      node->left_ = _avl_left_rotate(node->left_);
      node = _avl_right_rotate(node);
    }
  }
  return node;
}

template<typename T>
avl_tree<T>& avl_tree<T>::insert(T val)
{
  root_ = insert(val, root_);
  return *this;
}

template<typename T>
avl_tree<T>& avl_tree<T>::insert(std::vector<T>& vals)
{
  for (auto& val : vals)
    insert(val);
  return *this;
}

template<typename T>
bool avl_tree<T>::find(T val, avl_tree_node<T>* node)
{
  if (!node) return false;

  if (node->height_ == 0) {
    return false;
  } else {
    int cond = val > node->val_;
    if (cond > 0) {
      if (node->right_->height_ != 0) {
      }
      return find(val, node->right_);
    } else if (cond < 0) {
      if (node->left_->height_ != 0) {
      }
      return find(val, node->left_);
    } else {
      return true;
    }
  }
}

template<typename T>
bool avl_tree<T>::find(T val) {
  return find(val, root_);
}

template<typename T>
void avl_tree<T>::map(T (*f)(T), avl_tree_node<T>* node) {
  if (node == nullptr) {
  } else {
    node->val_ = f(node->val_);
    map(f, node->left_);
    map(f, node->right_);
  }
}

template<typename T>
avl_tree<T>& avl_tree<T>::map(T (*f)(T)) {
  map(f, root_);
  return *this;
}


template<typename T>
avl_tree_node<T>* avl_tree<T>::remove(
    T val, avl_tree_node<T>* node,
    avl_tree_node<T>** parent1, avl_tree_node<T>** parent2
)
{
  int balance = 0;
  bool skip_search = false;

  if (!node)
    return node;

  if (node->val_ == val && !parent2) {
    if (node->left_ && node->right_) {
      remove(val, node->right_, parent1, &(node->right_));
      skip_search = true;
    } else {
      *parent1 = node->left_ ? node->left_ : node->right_;

      node->left_ = node->right_ = nullptr;
      delete node;

      return *parent1;
    }
  }

  if (!skip_search) {
    if (!parent2) {
      avl_tree_node<T> ** tmp = val < node->val_ ? &node->left_ : &node->right_;
      remove(val, *tmp, tmp, nullptr);
    } else {
      avl_tree_node<T> ** tmp = node->left_ ? &node->left_ : nullptr;
      if (tmp) {
        remove(val, *tmp, parent1, tmp);
      } else {
        (*parent1)->val_ = node->val_;
        *parent2 = nullptr;
        delete node;
        return nullptr;
      }
    }
  }

  int nlh = node->left_  ? node->left_->height_  : 0;
  int nrh = node->right_ ? node->right_->height_ : 0;
  node->height_ = 1 + std::max(nlh, nrh);
  balance = nlh - nrh;

  if (balance < -1) {
    T val = node->right_->right_
      ? node->right_->right_->val_
      : node->right_->left_->val_;
    if (val < node->right_->val_)
      node->right_ = _avl_right_rotate(node->right_);
    node = _avl_left_rotate(node);
  }

  if (balance > 1) {
    T val = node->left_->left_
      ? node->left_->left_->val_
      : node->left_->right_->val_;
    if (val >= node->left_->val_)
      node->left_ = _avl_left_rotate(node->left_);
    node = _avl_right_rotate(node);
  }

  return node;
}

template<typename T>
avl_tree<T>& avl_tree<T>::remove(T val) {
  root_ = remove(val, root_, &root_, NULL);
  return *this;
}

}

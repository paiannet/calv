INC_DIR  := ./include
CC       := gcc
CXX      := g++
CCFLAGS  ?= -O3 -march=native -mavx2 -fopenmp -I$(INC_DIR)
CXXFLAGS ?= $(CCFLAGS)

ifdef DEBUG
  DEBUG_FLAGS := -g3
  BINDIR   := debug
else
  DEBUG_FLAGS :=
  BINDIR   := release
endif

TARGETS  := $(addprefix $(BINDIR)/,$(subst .cc,,calv.cc $(wildcard avl-*.cc)))

.PHONY: all
all: $(TARGETS)

$(BINDIR):
	mkdir -p $(BINDIR)

$(BINDIR)/calv: calv.cc page-map.cc avl-tree.cc avl-df.cc avl-bf.cc | $(BINDIR)
	$(CXX) $(DEBUG_FLAGS) $(CXXFLAGS) -o $@ $^

$(BINDIR)/avl-%: avl-%.cc page-map.cc | $(BINDIR)
	$(CXX) $(DEBUG_FLAGS) -DSTANDALONE $(CXXFLAGS) -o $@ $^

.PHONY: clean-release
clean-release:
	$(RM) release/*

.PHONY: clean-debug
clean-debug:
	$(RM) debug/*

.PHONY: clean
clean:
	$(RM) release/* debug/*
